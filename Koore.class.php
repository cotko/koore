<?php





/**
 * Koore class
 */
class Koore{
	
	
	private static $koore;
	
	private $base_path;
	
	private $dbHelper;
	
	private $mvcHandler;
	
	private $Lng;
	
	private $session;
	
	// will hold callbacks for calling when router registration should be run
	private static $REGISTER_CONTROLLER_CALLBACKS = array();
	
	/**
	 * TODO: fix this, mvc should tell wehether to surpress them or not
	 * and also they could be 'globaly' locked to surpress via settings if on production etc
	 * and they should be turnable on/off..
	 * @var unknown_type
	 */
	private static $surpressErrors = false;
	public static function surpressErrors($yes){
		self::$surpressErrors = $yes ? true:false;
	}
	
	
	private static $applicationDirName = "application";
	
	public function __construct() {
	}
	
	public static function callOnRequestRouterAvailable($callback) {
		self::$REGISTER_CONTROLLER_CALLBACKS[] = $callback;
	}
	
	/**
	 * starts the framework
	 */
	public static function start(){
		
		self::init();
		
		//start the mvc
		try{
			
			
			
			$wrapper = self::$koore->mvcHandler->start();
			
			//now dump all data
			$wrapper->dumpRendered();
			
		}catch(Exception $e){
			Debugger::addException($e);
		}
		//self::$koore->Lng->dump();
		
		//self::$koore->mvcHandler->dump();
		
		self::destroy();
		
	}
	
	
	/**
	 * starts the framework for services
	 */
	public static function startJsonService(){
		self::init();
		
		//start the mvc
		try{
			$wrapper = self::$koore->mvcHandler->start();
			
			
			//now dump all data
			$wrapper->dumpRendered();
			
		}catch(Exception $e){
			Debugger::addException($e);
		}
		//self::$koore->Lng->dump();
		
		//self::$koore->mvcHandler->dump();
		
		self::destroy();
	}
	
	
	/**
	 * starts the whole thing
	 */
	private static function init(){
		
		$koore = new Koore();
		
		//set base directory for inclusions
		$koore->base_path = dirname(__FILE__);
		
		//include the Loader class which can autoload all needed classes
		include( $koore->base_path . "/loader/KLoader.class.php");
		
		try{
			//set use caches for KLoader
			KLoader::init($koore->base_path, self::$applicationDirName, defined("IN_PRODUCTION") ? !!IN_PRODUCTION : true);
			
		}catch(Exception $e){
			Debugger::addException($e);
		}
		
		//start the session
		$koore->session = new Session();
		
		
		//handle errors
		$koore->convertErrorsToExceptions();
		
		//create DB class and connect it to database
		$koore->dbHelper = new DBHelper( DB_HOST, DB_USER, DB_PASS, DB_NAME );
		
		//let dbhelper automatically open the connection if it actually gets the request for query
		//$koore->dbHelper->openConnection();
		
		//set language
		try{
			$lngImplementation = defined('_K_LNG_IMPLEMENTATION') && _K_LNG_IMPLEMENTATION == 'FILE' ? Lng::LNG_FILE : Lng::LNG_DB;
			$koore->Lng = new Lng();
			$koore->Lng->init($lngImplementation);
			//$koore->Lng->init(false);
		}catch(Exception $e){
			Debugger::addException($e);
		}
		
		//set the mvc hanlder
		$koore->mvcHandler = new MVCHandler( $koore, realpath($koore->base_path . "/../".self::$applicationDirName) );
		
		// register routers if any
		foreach(self::$REGISTER_CONTROLLER_CALLBACKS as $callback) {
			$callback();
		}
		
		self::$koore = &$koore;
		
	}
	
	
	/**
	 * before ending script, do some cleanup
	 */
	private static function destroy(){
	
		//store any not translated words
		try{
			self::$koore->Lng->onExit();
		}catch(Exception $e){
			Debugger::addException($e);
		}
		//close the db connection, if it ws opened
		self::$koore->dbHelper->closeConnection();
		
		
		//tell KLoader that scripts is ending
		KLoader::shutDown();
		
		/**
		 * TODO: lalal fix, see upper todo..
		 */
		if( ! self::$surpressErrors )
			htmlWrapper::dumpErrors( Debugger::getErrors() );
	}
	
	
	/**
	 * this is called by classes which want to redirect etc. to tell koore to celan up
	 */
	public function stopAndClean(){
		
		self::destroy();
		
	}
	
	
	/**
	 * convert all php errors to Exceptions
	 */
	private function convertErrorsToExceptions(){
		set_error_handler( 
			create_function('$errno, $errstr, $errfile, $errline, array $errcontext', '{
				// error was suppressed with the @-operator
				if (0 === error_reporting()) {
					return false;
				}
				//throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
				Debugger::addException( new ErrorException($errstr, 0, $errno, $errfile, $errline) );
			}') 
		);
	}
	
	
	
	private function includeConfigs(){
		
		$configs = array_filter( rglob( $this->base_path, "*.config.php"), 'is_file');
		
		foreach( $configs as $config )
			require( $config );
	}
	
	
	private function includeClasses(){
		$classes = array_filter( rglob( $this->base_path, "*.class.php"), 'is_file');
		
		foreach( $classes as $class )
			if( basename($class)!=get_class($this).".class.php" )
				require( $class );
	}
	
	
	
}



