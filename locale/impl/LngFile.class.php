<?php

/**
 * localization class
 * uses files with language tranlsations in arrays
 */
class LngFile extends ALng {
	
	
	/*
	 * currently selected language like en_UK
	 */
	private static $selected_language_code = "undefined";
	
	/*
	 * currently selected language like en
	 */
	private static $selected_language_short_code = "undefined";
	
	/*
	 * valid lanugages
	 */
	private static $available_languages	= array();
	
	/*
	 * array of translations arrays
	 */
	private static $locales				= array();
	
	/*
	 * which locales didnt have key-value set for some translations
	 * and will be updated therefore
	 */
	private static $updateLanguageFiles	= array();
	
	/*
	 * result of querying database for locales
	 */
	private static $query_locale_reslut 	= array();
	
	
	
	
	
	public function __construct(){
	
	}
	protected static function getTable() {
		return 'koore_locale_file';
	}
	
	/**
	 * init the Lng
	 */
	public function init(){
		$locales = self::getQueryLocales();
		
		$warnings = "";
		
		foreach( $locales as $locale ){
			
			if( is_file($locale['locale_file']) ){
				
				unset($translations);
				
				// check the file syntax
				$status = 0;
				$ret;
				exec("php -l " . $locale['locale_file'], $ret, $status);
				
				if( $status != 0 ) { //syntax error?
					$warnings .= "- Lng warning: syntax error in language file: '$locale[locale_file]' (".print_r($ret,true).")<br>\n";
				} else{ //ok, valid php file
					
					include($locale['locale_file']);
					
					if( ! isset($translations) || ! is_array($translations) ) { //is valid language file?
						$warnings .= "- Lng warning: can't find translations array in file: '$locale[locale_file]'<br>\n";
					} else{ //ok valid language file..
					
						if( ! isset(self::$available_languages[$locale['code_short']]) )
							self::$available_languages[$locale['code_short']] = array();
						
						ksort($translations);
						self::$available_languages[$locale['code_short']][$locale['id_locale']]	= $locale['code'];
						self::$locales[$locale['code']]											= $translations;
						
						//first language in returned array is default as it was ordered by default field desc
						if( self::getLngCode() == 'undefined' )
							self::setLng($locale['code']);
						
					}
				}
			}
			else
				$warnings .= "- Lng warning: not found the language file: $locale[code] = '$locale[locale_file]'<br>\n";
				
		}
		
		unset($translations);
		
		if( $warnings != "" )
			throw new Exception( $warnings );
		
		
	}
	
	public static function translate( $key, $automaticallyAddKeyIFNotFound=true ){
		if( array_key_exists( $key, self::$locales[self::getLngCode()] ) )
			return self::$locales[self::getLngCode()][$key];
		
		//set this language for updating but avoid saving empty keys
		else if( $automaticallyAddKeyIFNotFound && trim($key) != '' ){
			self::$updateLanguageFiles[self::getLngCode()] = true;
			
			//add new key as translation was not found
			self::$locales[self::getLngCode()][$key] = $key;
		}
		
		//return not translated key
		return $key;
	}
	
	protected static function getQueryLocales($forceReload = false) {
		if(! self::$query_locale_reslut || $forceReload)
			self::$query_locale_reslut = DBHelper::fetch("select * from ". self::getTable() ." where active = 1 order by `default` desc", "code");
		return self::$query_locale_reslut;
	}
	
	public static function getLngShortCode() {
		return self::$selected_language_short_code;
	}
	
	public static function getLngCode() {
		return self::$selected_language_code;
	}
	
	public static function getAvailableLngs() {
		return self::$available_languages;
	}
	
	protected static function _setLngShortCode($shortCode) {
		self::$selected_language_short_code = $shortCode;
	}
	
	protected static function _setLngCode($code) {
		self::$selected_language_code = $code;
	}
	
	/**
	 * store modified lanugages if any
	 */
	public function onExit(){
		$warnings = '';
		
		foreach( self::$updateLanguageFiles as $lng=>$store ){
			if( $store ){
				$file	= self::$query_locale_reslut[$lng]['locale_file'];
				$code	= self::$query_locale_reslut[$lng]['code'];
				
				try{
					ksort(self::$locales[$code]);
					file_put_contents( $file, "<?php\n\n" . '$translations = ' . var_export(self::$locales[$code], true) . ';' ) ;
				}catch(Exception $e){
					$warnings .= "-Lng storeLanguagesIfTheyWereUpdated warning: " . $e->getMessage() . "<br>\n";
				}
				
			}
		}
		
		if( $warnings != "" )
			throw new Exception($warnings);
	}
	
	
	/**
	 * sets the array of translations for a language	
 	 * @param string $languageCode (ie en_GB / sl_SI etc..)
	 * @param array $tranlsations
	 * @return boolean true if such language exists false otherwise
	 */
	public function setTranslationsArray($languageCode, $tranlsations){
		if(! @self::$locales[$code] )
			return false;
		else{
			ksort($translations);
			self::$locales[$languageCode] = $translations;
			self::$updateLanguageFiles[$languageCode] = true;
		}
	}
	
	/**
	 * sets the translation for some word for specified language
	 * @param string $lng locale code, if it is short (en) all languages under this code will be set (like en_GB, en_US), or it can be full, like sl_SI
	 * @param string $key
	 * @param string $value
	 * @return boolean true if successfully set or false if language not found
	 */
	public static function setTranslation($key, $value, $lng){
		$lng 		= trim($lng);
		$code 		= "";
		$code_short	= "";
		
		if( strlen($lng) > 2 ){
			$code 		= $lng;
			$t 			= explode('_', $lng);
			$code_short	= $t[0];
		}
		else{
			$code_short	= $lng;
		}
		
		//checking for example for 'en'
		if( ! array_key_exists($code_short, self::$available_languages) )
			return false;
		
		//set translation for the language
		if( in_array($code, self::$available_languages[$code_short]) ){
			self::$locales[$code][$key] = $value;
			self::$updateLanguageFiles[$code] = true;
		}
		//else set this translation for all languages int this 'language family'
		else{
			foreach(self::$available_languages[$code_short] as $lngid=>$lngcode){
				self::$locales[$lngcode][$key] = $value;
				self::$updateLanguageFiles[$lngcode] = true;
			}
		}
		
		return true;
	}
	
	
	/**
	 * returns the translations for specified language
	 * @param mixed $languageCode array if translations exist aor null if no such language
	 */
	public static function getTranslationsForLanguage($languageCode){
		if(! @self::$locales[$languageCode] )
			return null;
		return  self::$locales[$languageCode];
	}
	
}

