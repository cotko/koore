<?php

/**
 * localization class
 * uses database for getting the translations
 */
class LngDB extends ALng {
	
	
	/*
	 * currently selected language like en_UK
	 */
	private static $selected_language_code = "undefined";
	
	/*
	 * currently selected language like en
	 */
	private static $selected_language_short_code = "undefined";
	
	/*
	 * valid lanugages
	 */
	private static $available_languages	= array();
	
	/*
	 * array of translations arrays
	 */
	private static $locales				= array();
	
	/*
	 * which locales didnt have key-value set for some translations
	 * and will be updated therefore
	 */
	private static $updateLanguageFiles	= array();
	
	/*
	 * result of querying database for locales
	 */
	private static $query_locale_reslut 	= array();
	
	/*
	 * table with translations
	 */
	private static $TRANSLATION_TABLE = 'koore_translation';
	
	
	
	public function __construct(){
	
	}
	protected static function getTable() {
		return 'koore_locale_db';
	}
	
	/**
	 * init the Lng
	 */
	public function init(){
		$locales = self::getQueryLocales();
		
		foreach( $locales as $locale ){
			
			if( ! isset(self::$available_languages[$locale['code_short']]) )
				self::$available_languages[$locale['code_short']] = array();
			
			self::$available_languages[$locale['code_short']][$locale['id_locale']]	= $locale['code'];
			
			//first language in returned array is default as it was ordered by default field desc
			if( self::getLngCode() == 'undefined' )
				self::setLng($locale['code']);
			
		}
	}
	
	public static function translate( $key, $automaticallyAddKeyIFNotFound=true ){
		$transRow = DBHelper::fetchOne(
			DBHelper::buildQuery(
				'select translation from `' . self::$TRANSLATION_TABLE . '` where id_locale={id_locale} and `key`={key}',
				array('id_locale' => self::getLngId(), 'key' => $key)
			)
		);
		if(! $transRow) {
		
			// insert into db if requested
			if($automaticallyAddKeyIFNotFound)
				DBHelper::insertOne(self::$TRANSLATION_TABLE, array('id_locale' => self::getLngId(), 'key' => $key, 'translation' => $key));
			
			
			return $key;
		}
		return $transRow['translation'];
	}
	
	protected static function getQueryLocales($forceReload = false) {
		if(! self::$query_locale_reslut || $forceReload)
			self::$query_locale_reslut = DBHelper::fetch("select * from ". self::getTable() ." where active = 1 order by `default` desc", "code");
		return self::$query_locale_reslut;
	}
	
	public static function getLngShortCode() {
		return self::$selected_language_short_code;
	}
	
	public static function getLngCode() {
		return self::$selected_language_code;
	}
	
	public static function getAvailableLngs() {
		return self::$available_languages;
	}
	
	protected static function _setLngShortCode($shortCode) {
		self::$selected_language_short_code = $shortCode;
	}
	
	protected static function _setLngCode($code) {
		self::$selected_language_code = $code;
	}
	
}

