<?php

/**
 * abstract localization class
 * sets default language and timezone
 * gets data from database!!
 */
abstract class ALng {
	
	public function __construct(){
	
	}
	
	/**
	 * should be implemented by a subclass
	 * @return name of table to get locales from
	 */
	protected static function getTable() {
		return static::getTable();
		//throw new Exception( get_called_class() . "::translate() - not implemented!" );
	}
	
	/**
	 * abstract init called by framework to set up all needed params
	 */
	public abstract function init();
	
	/**
	 * should return the translation for this key
	 * @param string $key string to translate
	 * @param boolean $automaticallyAddKeyIFNotFound default true; it inserts new translation for this key, translation is the same as passed key
	 */
	public static function translate( $key, $automaticallyAddKeyIFNotFound=true ) {
		return static::translate($key, $automaticallyAddKeyIFNotFound=true);
		//throw new Exception( get_called_class() . "::translate() - not implemented!" );
	}
	
	/**
	 * should be implemented by a subclass
	 * fetches locales from database table
	 * @param $forceReload boolean true if force reload although it was fetched before
	 * @return array of rows in locale table
	 */
	protected static function getQueryLocales($forceReload = false) {
		return static::getQueryLocales($forceReload);
		//throw new Exception( get_called_class() . "::getQueryLocales() - not implemented!" );
	}
	
	/**
	 * @return selected short lng code 
	 */
	public static function getLngShortCode() {
		return static::getLngShortCode();
		//throw new Exception( get_called_class() . "::getLngShortCode() - not implemented!" );
	}
	
	/**
	 * @return selected lng code 
	 */
	public static function getLngCode() {
		return static::getLngCode();
		//throw new Exception( get_called_class() . "::getLngCode() - not implemented!" );
	}
	
	/**
	 * @return selected lng id 
	 */
	public static function getLngId() {
		return self::getDBParamByKey('id_locale');
	}
	
	/**
	 * @return available languages array
	 */
	public static function getAvailableLngs() {
		return static::getAvailableLngs();
		//throw new Exception( get_called_class() . "::getAvailableLngs() - not implemented!" );
	}
	
	/**
	 * set the selected language
	 * @param $shortCode select language
	 */
	protected static function _setLngShortCode($shortCode) {
		static::_setLngShortCode($shortCode);
		//throw new Exception( get_called_class() . "::_setLngShortCode() - not implemented!" );
	}
	
	/**
	 * set the selected language
	 * @param $code select language
	 */
	protected static function _setLngCode($code) {
		static::_setLngCode($code);
		//throw new Exception( get_called_class() . "::_setLngCode() - not implemented!" );
	}
	
	/**
	 * @param $key table's column name
	 * @return table column value for this column name
	 */
	public static function getDBParamByKey($key) {
		$query_locale_reslut = self::getQueryLocales();
		return $query_locale_reslut[self::getLngCode()][$key];
	}
	
	/**
	 * called by framework at the end of request handling
	 * can be used for various operations if needed
	 * (saving translations to file if this is file based translation)
	 */
	public function onExit() {
	
	}
	
	
	/**
	 * set the languge 
	 * @param $lng can be just short 'en' or more specific like 'en_UK'
	 * @param $phpSetLng if also set the php's locale
	 * @param $phpSetTimezone if also set the php's timezone
	 */
	public static function setLng( $lng, $phpSetLng=true, $phpSetTimezone=true ){
		// guess if full or short code is passed
		$lng 		= trim($lng);
		$code 		= "";
		$code_short	= "";
		
		if( strlen($lng) > 2 ){
			$code 		= $lng;
			$t 			= explode('_', $lng);
			$code_short	= $t[0];
		}
		else{
			$code_short	= $lng;
		}
		
		$available_languages = self::getAvailableLngs();
		
		//checking for example for 'en'
		if( ! array_key_exists($code_short, $available_languages) )
			return false;
		
		// set this short code as selected
		self::_setLngShortCode($code_short);
		
		// set full code
		if( in_array($code, $available_languages[$code_short]) ) { //checking for example for 'en'=>array('en_UK')
			self::_setLngCode($code);
		} else{ //select first language for example for 'en'
			$code	= current($available_languages[$code_short]);
			self::_setLngCode($code);
		}
		
		//also set php locale for this language
		if( $phpSetLng ){
			setlocale(LC_ALL, $code);
		}
		//set the timezone
		if( $phpSetTimezone && self::getTimezone() != "" )
			date_default_timezone_set(self::getTimezone());
		
	}
	
	/**
	 * get the currently selected language
	 * @return array(lng=>lng, id=>lngId, ... other data..)
	 */
	public static function getLng(){
		$lCode = self::getLngCode();
		foreach(self::getQueryLocales() as $code => $lngData)
			if($lCode == $code)
				return $lngData;
		// not found, throw fuckeen exception!
		throw new Exception( "Lng->getLng(): no language found for short code: $lShortCode, code: $lcode" );
	}
	
	public static function getTimezone(){
		return self::getDBParamByKey('timezone');
		//return self::$query_locale_reslt[self::$selected_language]['timezone'];
	}
	
	public static function getMysqlDateFormat(){
		return self::getDBParamByKey('date_format_mysql');
	}
	
	public static function getPHPDateFormat(){
		return self::getDBParamByKey('date_format_php');
	}
	
	public static function getJavascriptDateFormat(){
		return self::getDBParamByKey('date_format_javascript');
	}
	
	public static function dump(){
		print_r( array(
			"query locales" => self::getQueryLocales(),
			"lng" => self::getLng(),
			"getLngShortCode" => self::getLngShortCode(),
			"getLngCode" => self::getLngCode(),
			"getAvailableLngs" => self::getAvailableLngs()
		));
	}
}

