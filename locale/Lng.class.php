<?php

/**
 * localization wrapper class 
 */
class Lng {
	
	const LNG_DB = 1;
	const LNG_FILE = 2;
	
	private static $LNG;
	
	private static $LNG_NAME = "";
	
	public function __construct(){
	
	}
	
	public function init($which){
		switch($which) {
		case self::LNG_DB:
			self::$LNG = new LngDB();
		break;
		case self::LNG_FILE:
			self::$LNG = new LngFile();
		break;
		default:
			throw new Exception( "Lng->__construct(): not available language implementation for: $which" );
		break;
		}
		self::$LNG->init();
		self::$LNG_NAME = get_class(self::$LNG);
	}
	
	private static function getCallableLng($methodName) {
		return self::$LNG_NAME . "::" . $methodName;
	}
	
	public function onExit() {
		self::$LNG->onExit();
	}
	
	public static function translate( $key, $automaticallyAddKeyIFNotFound=true ) {
		return call_user_func(self::getCallableLng('translate'), $key, $automaticallyAddKeyIFNotFound);
	}
	public static function getLngShortCode() {
		return call_user_func(self::getCallableLng('getLngShortCode'));
	}
	
	public static function getLngCode() {
		return call_user_func(self::getCallableLng('getLngCode'));
	}
	
	public static function getAvailableLngs() {
		return call_user_func(self::getCallableLng('getAvailableLngs'));
	}
	
	public static function setLng( $lng, $phpSetLng=true, $phpSetTimezone=true ){
		return call_user_func(self::getCallableLng('setLng'), $lng, $phpSetLng, $phpSetTimezone);
	}
	
	public static function getLng(){
		return call_user_func(self::getCallableLng('getLng'));
	}
	
	public static function getTimezone(){
		return self::getDBParamByKey('timezone');
	}
	
	public static function getMysqlDateFormat(){
		return self::getDBParamByKey('date_format_mysql');
	}
	
	public static function getPHPDateFormat(){
		return self::getDBParamByKey('date_format_php');
	}
	
	public static function getJavascriptDateFormat(){
		return self::getDBParamByKey('date_format_javascript');
	}
	
	public static function getDBParamByKey($key){
		return call_user_func(self::getCallableLng('getDBParamByKey'), $key);
	}
	
	public static function dump(){
		call_user_func(self::getCallableLng('dump'));
	}
	
}

