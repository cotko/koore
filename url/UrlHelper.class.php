<?php


/**
 * helper .. does nothing but starts the session for now..
 * @author mitja
 *
 */
class UrlHelper{
	
	const REDIRECT_301_MOVED_PERMANENTLY = '301 Moved Permanently';
	const REDIRECT_302_MOVED_TEMPORARELY = ''; //default php header for redirection
	
	private static $koore;
	
	private static $controllerName;
	private static $actionName; 
	
	private static $USE_ROUTER = true;
	
	public function __construct($koore, $controllerName, $actionName){
		
		self::$koore = $koore;
		
		self::$controllerName = $controllerName;
		self::$actionName = $actionName;
		
	}
	
	/**
	 * whether to use registered reoutes to translate controller names and actions
	 * @param $useIt
	 */
	public static function useRouter($useIt) {
		self::$USE_ROUTER = !! $useIt;
	}
	
	/**
	 * builds the url
	 * $controllerOrParams can be array ( key-value pairs for params) or a controller name
	 * $actionOrParams can be array ( key-value pairs for params) or a action name
	 * $params can be array ( key-value pairs for params)
	 * @param array or string $controllerOrParams
	 * @param array or string $actionOrParams
	 * @param array or string $params
	 * @param mixed $prependLanguageCode if auto (default), language code will be appended if current request uri has it or not otherwise. If true/false it will be or not be prepended
	 * @param boolean $forceRouterUsage whether to translate controller name and action to the language set by @param $shortLanguageCodeForFouter
	 * @param string $shortLanguageCodeForFouter short lng code to lookup the controller and action names, if auto (default) current language code will be used
	 */
	public static function buildUrl( $controllerOrParams="index", $actionOrParams="index", $params="", $prependLanguageCode = 'auto', $forceRouterUsage = false, $shortLanguageCodeForFouter = 'auto' ){
		$p = $params;
		$c = self::$controllerName;
		$a = self::$actionName;
		
		if( is_array( $controllerOrParams ))
			$p = $controllerOrParams;
		else if( $controllerOrParams != "" ){
			if( $controllerOrParams != "index" )
				$c = $controllerOrParams;
			else $c = "";
		}
			
		
		if( is_array( $actionOrParams ))
			$p = $actionOrParams;
		else if( $actionOrParams != "" ){
			if( $actionOrParams != "index" )
				$a = $actionOrParams;
			else
				$a = "";
		}
		
		//if params are set, we can't use empty strings for index controller and/or action
		if( is_array($p) && sizeof($p) > 0 ){
			$c = $c == "" ? "index" : $c;
			$a = $a == "" ? "index" : $a;
		}
		
		
		if($forceRouterUsage || self::$USE_ROUTER) {
			$lcode = $prependLanguageCode == 'auto' || ! is_string($prependLanguageCode) ? 'auto' : $prependLanguageCode; 
			if($a != '' && $a != 'index')
				$a = RequestRouter::lookupActionName($a, $c == '' ? 'auto' : $c, $lcode);
			if($c != '' && $c != 'index')
				$c = RequestRouter::lookupControllerName($c, $lcode);
		}
		
		$codeLng = '';
		if($prependLanguageCode !== false) {
			if($prependLanguageCode == 'auto' && RequestHandler::isLanguageSet())
				$codeLng = Lng::getLngShortCode();
			else if($prependLanguageCode != 'auto')
				$codeLng = trim($prependLanguageCode, ' /');
			if($codeLng != '')
				$codeLng .= '/';
		}
		
		return '/' . $codeLng .  trim("$c/$a/" . self::getParamsUri($p), '/' );
	}
	
	
	
	private static function getParamsUri( $params ){
		
		$r = "";
		if( is_array($params ) ){
			foreach( $params as $k=>$v )
				$r .= "$k/$v/";
		}
		return trim($r, '/');
	}
	
	
	
	
	/**
	 * alias for redirectTo
	 */
	public static function redirect( $url, $redirectMode=false ){
		self::redirectTo($url, $redirectMode);
	}
	/**
	 * redirect and exit
	 */
	public static function redirectTo( $url, $redirectMode=false ){
		switch($redirectMode){
			case self::REDIRECT_301_MOVED_PERMANENTLY:
				$redirectMode = REDIRECT_302_MOVED_TEMPORARELY;
			break;
			case REDIRECT_302_MOVED_TEMPORARELY:
			default:
				$redirectMode = '';
			break;
		}
		//first tell koore we're gonna redirect, so it can clean up
		ob_start();
		self::$koore->stopAndClean();
		ob_end_clean();
		
		//then redirect
		header("Location: $url $redirectMode");
		exit(0);
		
	}
	
	
	
	
	
	
}