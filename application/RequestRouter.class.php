<?php

/**
 * Class which can register request routers
 *  it routes controller and action names for different languages
 *  to the default names
 */
class RequestRouter{
	
	
	// TODO: proper methods for building of urls / or changing url helper for that..
	private static $routers = array();
	
	
	/**
	 * registers a router for specified language
	 * @param $router should be:
	 *  array( 
	 * 		'translatedControllerName' => array(
	 *			'controller' => 'originalControllerName'
	 *			'actions' => array(
	 *				'translatedActionName1' => 'originalActionName1',
	 *				...
	 *			)
	 *		),
	 *		...
	 *	)
	 */
	public static function registerRouter($lngShortCode, $router) {
		self::$routers[$lngShortCode] = $router;
	}
	
	/**
	 * registers array of routers, indexed by: 
	 *	array(
	 *		lngShortCode1 => router1,
	 *		lngShortCode2 => router2,
	 *	)
	 */
	public static function registerRouters($routers) {
		foreach($routers as $lngShortCode => $router) {
			self::registerRouter($lngShortCode, $router);
		}
	}
	
	/**
	 * trys to find a controller name in registered routers
	 * @param $controllerName controller name to be resolved, if default ('auto'), RequestHandler will be used to get the parsed controller
	 * @param $lngShortCode to be used for registered routers search, if 'auto' (default), then Lng class will be used to get the language
	 * @return original controller name if found or $controllerName if not (if $controllerName is 'auto', parsed $controllerName will be returned)
	 */
	public static function resolveControllerName($controllerName = 'auto', $lngShortCode = 'auto') {
		if($lngShortCode == 'auto')
			$lngShortCode = Lng::getLngShortCode();
		if($controllerName == 'auto')
			$controllerName = RequestHandler::getControllerName();
		return 
			isset(self::$routers[$lngShortCode]) && 
			isset(self::$routers[$lngShortCode][$controllerName]) ? 
			self::$routers[$lngShortCode][$controllerName]['controller'] : $controllerName;
	}
	
	/**
	 * trys to find an action name in registered routers for specifiend controller
	 * @param $controllerName controller name to be resolved, if default ('auto'), RequestHandler will be used to get the parsed controller
	 * @param $actionName action name to be resolved, if default ('auto'), RequestHandler will be used to get the parsed action
	 * @param $lngShortCode to be used for registered routers search, if 'auto' (default), then Lng class will be used to get the language
	 * @return original controller name if found or $translatedName if not
	 */
	public static function resolveActionName($actionName = 'auto', $controllerName = 'auto', $lngShortCode = 'auto') {
		if($lngShortCode == 'auto')
			$lngShortCode = Lng::getLngShortCode();
		if($controllerName == 'auto')
			$controllerName = RequestHandler::getControllerName();
		if($actionName == 'auto')
			$actionName = RequestHandler::getActionName();
			
		return 
			isset(self::$routers[$lngShortCode]) && 
			isset(self::$routers[$lngShortCode][$controllerName]) &&
			isset(self::$routers[$lngShortCode][$controllerName]['actions'][$actionName]) ? 
			self::$routers[$lngShortCode][$controllerName]['actions'][$actionName] : $actionName;
	}
	
	/**
	 * trys to find the routed controller name for original controller
	 * @param $controllerName controller name to be translated if 'auto', current controller name will be used
	 * @param $lngShortCode to be used for registered routers search, if 'auto' (default), then Lng class will be used to get the language
	 * @returns translated controller name or $controllerName if not found
	 */
	public static function lookupControllerName($controllerName = 'auto', $lngShortCode = 'auto') {
		if($lngShortCode == 'auto')
			$lngShortCode = Lng::getLngShortCode();
		if($controllerName == 'auto')
			$controllerName = RequestHandler::getControllerName();
		foreach(self::$routers[$lngShortCode] as $translatedController => $controller)
			if($controller['controller'] == $controllerName)
				return $translatedController;
		return $controllerName;
	}
	
	
	/**
	 * trys to find the routed action name for original controller
	 * @param $controllerName controller name to be translated
	 * @param $actionName action name to be resolved, if default ('auto'), RequestHandler will be used to get the parsed action
	 * @param $lngShortCode to be used for registered routers search, if 'auto' (default), then Lng class will be used to get the language
	 * @returns translated controller name or $controllerName if not found
	 */
	public static function lookupActionName($actionName = 'auto', $controllerName = 'auto', $lngShortCode = 'auto') {
		if($lngShortCode == 'auto')
			$lngShortCode = Lng::getLngShortCode();
		if($controllerName == 'auto')
			$controllerName = RequestHandler::getControllerName();
		if($actionName == 'auto')
			$actionName = RequestHandler::getActionName();
		foreach(self::$routers[$lngShortCode] as $translatedController => $controller)
			if($controller['controller'] == $controllerName)
				foreach($controller['actions'] as $translatedAction => $action)
					if($action == $actionName)
						return $translatedAction;
		return $actionName;
	}
}