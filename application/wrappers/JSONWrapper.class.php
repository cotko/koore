<?php


class JSONWrapper extends AWrapper{
	
	
	private $data;

	
	function __construct($data){
		$this->data = $data;
		//print_R($data);
	}
	
	
	
	public function dumpRendered(){
		
		
		header('Content-type: application/json');
		echo str_replace('>>>>"', "", str_replace('"<<<<', "", json_encode($this->data)));
		
		
	}
	
	/**
	 * encodes array of data and respects all values/keys which should not be quoted
	 * @see prepareForNoQuotes($val)
	 * @param array $data
	 */
	public static function json_encode($data){
		return str_replace("\/", "/", str_replace('>>!NOQUOTE>"', "", str_replace('"<!NOQUOTE<<', "", json_encode($data))));
	}
	
	/**
	 * when using json wrapper, array is converted using json_encode
	 * this method quotes all values and keys
	 * using this method, you can set value/key which will be unquoted (usefull for referenting to functions, variables etc..)
	 * @param string $val
	 */
	public static function prepareForNoQuotes($val){
		return "<!NOQUOTE<<$val>>!NOQUOTE>";
	}
}