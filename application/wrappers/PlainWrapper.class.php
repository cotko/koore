<?php


class PlainWrapper extends AWrapper{
	
	
	private $renderedViews;
	private $layout;
	private $isRendered;
	
	function __construct($renderedViews, $layout, $isRendered=false){
		$this->renderedViews	= $renderedViews;
		$this->layout			= $layout;
		$this->isRendered		= $isRendered;
	}
	
	
	
	public function dumpRendered(){
			
		header('Content-type: text/html');
		if( $this->isRendered )
			print($this->layout->show($this->renderedViews));
		else
			print($this->renderedViews);
	}
	
	
}