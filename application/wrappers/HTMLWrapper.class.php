<?php


class HTMLWrapper extends AWrapper{
	
	private static $docTypes = array(
		'loose' => '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
		'transitional' => '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
		'strict' =>  '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">'
	);
	private static $useDoctype = 'transitional';
	
	private $renderedViews;
	private $layout;
	
	function __construct($renderedViews, $layout){
		$this->renderedViews	= $renderedViews;
		$this->layout			= $layout;
	}
	
	
	/**
	 * which doctype to use: strict/transitional/loose
	 * @param string $type strict/transitional/loose
	 */
	public static function useDoctype($type){
		switch($type){
			case 'transitional':
				self::$useDoctype = 'transitional';
				break;
			case 'strict':
				self::$useDoctype = 'strict';
				break;
			default:
				self::$useDoctype = 'loose';
				break;
		}
	} 
	
	public function dumpRendered(){
		
		$rendered =$this->layout->show($this->renderedViews);
		 
		
		$this->dumpHTMLBeginning();
?>
	<body>
	

<?php echo $rendered ?>


	</body>	
<?php
		
		$this->dumpHTMLEnd();
	}
	
	
	private function dumpHTMLBeginning(){
		
		$script_tag_content = "";
		
		//javascripts
		foreach( $this->layout->getJScripts() as $s )
			$script_tag_content .= $s . "\n";
		
		//jquery javascripts
		if( sizeof($this->layout->getJQueries()) > 0 ){
			$script_tag_content .= "\n\n" . '$(document).ready(function() {';
			foreach( $this->layout->getJQueries() as $s )
				$script_tag_content .= $s . "\n";
			$script_tag_content .= "\n" . '});';
		}
		
//print doctype
echo self::$docTypes[self::$useDoctype];
?>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $this->layout->getTitle() ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $this->layout->getEncoding() ?>" />
		<meta http-equiv="CONTENT-LANGUAGE" content="<?php echo $this->layout->getContentLanguages() ?>" />
		<meta http-equiv="EXPIRES" content="<?php echo $this->layout->getExpires() ?>" />
		<meta name="description" content="<?php echo $this->layout->getDescriptions() ?>" />
		<meta name="keywords" content="<?php echo $this->layout->getKeywords() ?>" />
		
		<link rel="shortcut icon" href="<?php echo $this->layout->getShortcutIcon() ?>" />
		<link rel="apple-touch-icon" href="<?php echo $this->layout->getAppleTouchIcon() ?>" />
		<link rel="icon" type="image/vnd.microsoft.icon"  href="<?php echo $this->layout->getIcon() ?>" />
		
		<?php
		foreach( $this->layout->getJScriptPaths() as $s ){
			?><script type="text/javascript" language="javascript" src="<?php echo $s; ?>"></script>
		<?php
		}
		foreach( $this->layout->getCssPaths() as $s ){
			?><link rel="stylesheet" type="text/css" href="<?php echo $s; ?>"/>
		<?php
		}
		if( sizeof($this->layout->getCss()) > 0 ){
		
		?><style type="text/css">
<?php
			foreach( $this->layout->getCss() as $css ){
				echo $css . "\n\n";
			}
?>
		</style>
<?php
		}
?>
		
		
		<script type="text/javascript">
<?php echo $script_tag_content; ?>
		</script>
	</head>
<?php
	}
	
	
	private function dumpHTMLEnd(){
?>
</html>
<?php
	}
	
	
	public static function dumpErrors( $errors=array() ){
		if( ! is_array($errors) || sizeof($errors) < 1 )
			return;
		
		//$this->debugger->printErrors();
		echo '<div style="top:0; left:0; background: rgba(11, 133, 15, 0.9); text-shadow:1px 1px 0 black; color: white; z-index: 99999; height:25px; position: fixed; overflow:hidden;" onClick="if( this.style.height==\'25px\' ) this.style.height=\'\'; else this.style.height=\'25px\';"><div style="cursor:pointer; height: 25px; color: orange; font-weight: bold;">==== ERRORS  =======</div>';
		foreach( Debugger::getErrors() as $error )
			echo "* $error<br>";
		echo '<div style="color: orange; font-weight: bold;">=================</div></div>';
	}
	
	
	
}