<?php


class JavascriptWrapper extends AWrapper{
	
	
	private $data;

	
	function __construct($data){
		$this->data = $data;
		//print_R($data);
	}
	
	
	
	public function dumpRendered(){
		
		
		header('Content-type: text/javascript');
		print($this->data);
		
		
	}
	
	
}