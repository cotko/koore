<?php
/**
 * TODO: callable layout's acitons should have Action or sth at end of method name
 * because now they can be called via action in url!!!!!!!!!!!!!
 *  
 * @author mitja
 *
 */

class MVCHandler{
	
	private $wrapper;
	
	private $requestHandler;
	
	private $base_application_path = "";
	
	private $views			= array();
	
	private $urlHelper;
	
	public function __construct( $koore, $base_application_path ){
		
		$this->base_application_path = $base_application_path;
		
		$this->requestHandler = new RequestHandler( Lng::getAvailableLngs() );
		
		//make an urlhelper, so it will initialize itself and set some vars to be used by controllers, views etc..
		$this->urlHelper = new UrlHelper( $koore, $this->requestHandler->getControllerName(), $this->requestHandler->getActionName() );
		
		//$this->requestHandler->dump();
		
	}
	
	
	/**
	 * starts handling of MVC framwork
	 * starts mvc and returns the Wrapper class
	 * can currently be of type plain, json and html
	 */
	public function start(){
		
		//check if any valid language was set in url and set it
		if( $this->requestHandler->getLng() != "" )
			Lng::setLng( $this->requestHandler->getLng() );
		
		//collect the views
		$this->collectViews();
		
		
		//now run selected controller and its selected action
		$controller 	= $this->runController();
		$controllerName	= $controller['controllerName'];
		$actionName		= $controller['actionName'];
		$surpressedByOnBefore = $controller['surpressedByOnBefore'];
		$controller		= &$controller['controller'];
		$data			= $controller->data;
		$wrapper 		= "";
		$wrapperType	= $controller->getDataType();
		$renderedViews 	= "";
		
		//get the layout
		$layout = $this->getLayout( $controller->getLayout() );
			
		
		//check if suppress views and layouts..
		if( ! $controller->suppressViews() && ! $surpressedByOnBefore ){
				
			$views = array();
			
			//common views if controller is using them
			if( $controller->usesCommonViews() && array_key_exists("_common_", $this->views) )
				$views = array_merge($views, $this->views['_common_']);
				
			//common views for this controller if controller is using them
			if( $controller->usesCommonControllerViews() && @$this->views[$controllerName]['_common_'] )
				$views = array_merge($views, $this->views[$controllerName]['_common_']);	
			
			//controller's action views
			if( @$this->views[$controllerName][$actionName] )
				$views = array_merge( $views, $this->views[$controllerName][$actionName] );
			
			//print_r($views);	
			
			//now start all views for this controller
			if( sizeof($views) > 0 ){
				//create a sandbox for views and render them
				$vs = new ViewsSandbox($views, $data, $layout);
				$renderedViews = $vs->renderViews();
				
			}
			
			//now send the layer and rednered views to a Wrapper
			switch($wrapperType){
			case "html":
				$this->wrapper = new HTMLWrapper($renderedViews, $layout);
				break;
			case "json":
				$this->wrapper = new JSONWrapper($data);
				break;
			case "plain":
				$this->wrapper = new PlainWrapper($renderedViews, $layout, true);
				break;
			case "javascript":
				$this->wrapper = new JavascriptWrapper($data);
				break;
			default:
				throw new Exception("MVCHandler error: no wrapper for type '$wrapperType'");
				break;
			}
			
		}
		else{
			
			//now send the layer and rednered views to a Wrapper
			switch($wrapperType){
			case "json":
				$this->wrapper = new JSONWrapper($data);
				break;
			case "plain":
				$this->wrapper = new PlainWrapper($data, $layout, false);
				break;
			case "javascript":
				$this->wrapper = new JavascriptWrapper($data);
				break;
			default:
				throw new Exception("MVCHandler error: no wrapper for type '$wrapperType' for suppressed views");
				break;
			}
			
		}
			
		//stop
		$this->stop();
		
		return $this->wrapper;
	}
	
	
	/**
	 * called when stopping the scripts
	 */
	private function stop(){
		
	}
	
	
	/**
	 * start the correct controller and run the correct method
	 * returns the array of (controller, data (returned by controller), controllerName)
	 * @param $layout send layer to the controller
	 */
	private function runController(){
		$ret = array();
		
		
		//get the controller
		//$cn	= $this->requestHandler->getControllerName();
		$cn =RequestRouter::resolveControllerName();
		
		//security check; controler can be alphabetic and less than 33 characters in length!
		if(strlen($cn) > 32 || !preg_match('/^[A-Za-z]+$/',$cn)){
			//say it's not found, don't tell the actual error..
			throw new NoControllerException("MVCHandler error: no controller found", 0, $cn);
			return;
		}
		
		//$an = $this->requestHandler->getActionName();
		$an = RequestRouter::resolveActionName();
		
		//additinal security check: check the action name, it also has to be alphabetic and less than 33 chars long
		if(strlen($an) > 32 || !preg_match('/^[A-Za-z]+$/',$an)){
			//say it's not found, don't tell the actual error..
			throw new NoActionException("MVCHandler error: no action found", 0, $cn, $an);
			//throw new Exception("MVCHandler error: no action '".$this->requestHandler->getActionName()."' found in controller: " . $this->requestHandler->getControllerName());
			return;
		}
		
		
		//load and get the name of the class
		$c 	= KLoader::loadApplicationController($cn);
		
		if( ! $c ){//file was not found
			//throw new Exception("MVCHandler error: no controller found: " . $cn);
			throw new NoControllerException("MVCHandler error: no controller found", 0, $cn);
		}
		
		$ret['controllerName'] = $cn;
		
		//ok, init the controller
		$c 	= new $c();
		
		//check if controller extends AController class
		if( ! $c instanceof AController ){
			throw new Exception("MVCHandler error: controller doesn't extend AController class: " . get_class($c));
			return;
		}
		
		//action
		$m	= $an . "Action"; //all methods should have 'Action' appended; these are actions;
		$c->action = $an;
		
		//check if required action method exists in controller
		if( ! method_exists($c, $m) ){
			//throw new Exception("MVCHandler error: no action '".$this->requestHandler->getActionName()."' found in controller: " . $this->requestHandler->getControllerName());
			throw new NoActionException("MVCHandler error: no action found", 0, $cn, $an);
			return;
		}
		
		//ok, run the controller
		$tmpdata = $c->onBeforeControllerActionCall();
		//if returned value is not null, we only return this value without running other actions
		if( $tmpdata !== null ){
			$c->data = $tmpdata;
			$ret['surpressedByOnBefore'] = true;
		}
		//else run other functions
		else{
			$ret['surpressedByOnBefore'] = false;
			//store output from action into controller's $data variable 
	 		$c->data = $c->$m();
			$c->onAfterControllerActionCall();
		}
		$ret['controller'] = $c;
		$ret['actionName'] = /*$m*/ $an /*this is later used  for checking views for this action etc, so only return the action whithout Action 'safety' */;
		
		//return the array
		return $ret;
	}
	
	
	/**
	 * sets the desired layout
	 * checks if DEFAULT_LAYOUT is set and returns the layout
	 * else it takes first (or the only) available layout
	 * @param $overridenByController the layout name that was set in controller, if not set the default will be used
	 */
	private function getLayout( $overridenByController="" ){
		$l;
		
		$layoutName = $overridenByController != "" ? $overridenByController : ( defined("DEFAULT_LAYOUT") ? DEFAULT_LAYOUT : false );
		//if defined
		if( $layoutName != false ){
		
			$l = KLoader::loadApplicationLayout($layoutName);
			
			if( ! $l )
				throw new Exception("MVCHandler error: Layout '".$layoutName."' doesn't extist");
			
		}
		//find first one
		else{
			$layouts = KLoader::getLayouts();
			
			
			if( sizeof($layouts) < 1 )
				throw new Exception("MVCHandler no Layouts found!");
			
			$l = KLoader::loadApplicationLayout(current($layouts));
			
			if( ! $l )
				throw new Exception("MVCHandler error: Layout '".$layoutName."' doesn't extist");
		}
		
		//initialize
		$l = new $l;
		
		
		//check if layout extends ALayout
		if( ! $l instanceof ALayout )
			throw new Exception("MVCHandler error: layout doesn't extend Layout class: " . get_class($c));
		
		return $l;
	}
	
	
	
	/**
	 * collect the views
	 */
	private function collectViews(){
		$files = array_filter( rglob( $this->base_application_path . '/view', "*.phtml"), 'is_file');
		$views = array('_common_' => array() );
		
		foreach( $files as $file ){
			$a = explode('/', $file);
			$t = array();
			
			unset( $a[sizeof($a) - 1 ] ); //remove file from array, we're only interested in directories 
			while( next($a) && current($a) != 'view' );
				
			if( current($a) == 'view' ){
					
				$actionDir = "";
				$controllerDir = "";
				
				if( ! ($controllerDir = next($a)) ){
					if( ! @$views['_common_'] )
						$views['_common_'] = array();
						
					$views['_common_'][] = $file;
				}
				else if( ! ($actionDir = next($a)) ){
					if( ! @$views[$controllerDir] )
						$views[$controllerDir] = array();
					if( ! @$views[$controllerDir]['_common_'] )
						$views[$controllerDir]['_common_'] = array();
					$views[$controllerDir]['_common_'][] = $file;
				}
				else{
					if( ! @$views[$controllerDir] )
						$views[$controllerDir] = array();
					if( ! @$views[$controllerDir][$actionDir] )
						$views[$controllerDir][$actionDir] = array();
					$views[$controllerDir][$actionDir][] = $file;
				}
				
			}
		
		}
		$this->views = $views;
		//print_r($this->views);
	}
	
	
	
	
	
	
	private function debugException( $exception ){
		Degubber::addException($exception);
	}
	private function debugError( $error ){
		Degubber::addError($error);
	}
	
	
	public function dump(){
		print_r($this);
	}
	
}