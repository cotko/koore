<?php
class NoControllerException extends Exception {
	
	private $controllerName = '';
	public function __construct($message=null, $code=0, $controllerName=""){
		$message .= ", controller name='$controllerName'";
		parent::__construct($message, $code);
		$this->controllerName = $controllerName;
	}
	
	public function getControllerName(){
		return $this->controllerName;
	}
	
}