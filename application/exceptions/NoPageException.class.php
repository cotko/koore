<?php
class NoPageException extends Exception {
	
	private $controllerName = '';
	private $actionName = '';
	private $displayMessage = '';
	public function __construct($message=null, $code=0, $controllerName="", $actionName="", $displayMessage="no page found"){
		$this->controllerName = $controllerName;
		$this->actionName = $actionName;
		$message .= ", controller name='$controllerName', action name='$actionName'";
		parent::__construct($message, $code);
	}
	
	public function getDisplayMessage(){
		return $this->displayMessage;
	}
	
	public function getControllerName(){
		return $this->controllerName;
	}
	
	public function getActionName(){
		return $this->actionName;
	}
	
	
}