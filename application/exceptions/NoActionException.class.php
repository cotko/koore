<?php
class NoActionException extends Exception {
	
	private $controllerName = '';
	private $actionName = '';
	public function __construct($message=null, $code=0, $controllerName="", $actionName=""){
		$this->controllerName = $controllerName;
		$this->actionName = $actionName;
		$message .= ", controller name='$controllerName', action name='$actionName'";
		parent::__construct($message, $code);
	}
	
	public function getControllerName(){
		return $this->controllerName;
	}
	
	public function getActionName(){
		return $this->actionName;
	}
	
	
}