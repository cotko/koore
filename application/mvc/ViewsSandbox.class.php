<?php


/**
 * this is the class from which the views are included
 * it is a sandbox, so views can only access this class's variables
 */
class ViewsSandbox{
	
	/*
	 * objects to which the views will have the access
	 */
	private $data;
	private $layout;
	
	private $views;
	
	function __construct( $views, $data, $layout ){
		$this->views	= $views;
		$this->data		= $data;
		$this->layout	= $layout;
		
	}
	
	/**
	 * return array of rendered views
	 */
	public function renderViews(){
		
		//array for returning viewname-viewhtml
		$___ret__RET = array();
		
		ob_start();
		foreach( $this->views as $v ){
			
			//view's name
			$name = str_replace(".phtml", "", basename($v));
			include_once( $v );
			
			$___ret__RET[$name] = ob_get_contents();
			
			ob_clean();
		}
		ob_end_clean();
		
		return $___ret__RET;
	}
	
}
