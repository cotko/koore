<?php

abstract class ALayout{
	
	
	private static $jscript	= array();
	private static $jquery	= array();
	private static $css		= array();
	
	private static $jscript_path	= array();
	private static $css_path		= array();
	
	
	private static $title				= "No title.";
	private static $encoding			= "utf-8";
	private static $CONTENT_LANGUAGE	= array();
	private static $EXPIRES				= "";
	private static $description			= array();
	private static $keywords			= array();
	private static $SHORTCUT_ICON		= "";
	private static $ICON 				= "";
	private static $APPLE_TOUCH_ICON  	= "";
	
	
	/**
	 * when first time auto initialized, this will be set to true
	 * and if another instance of layou will be used, title, ecoding and other values that may be changed by controllers
	 * won't be overriden
	 */
	private static $_was_intialized		= false;
	
	
	/**
	 * must implement show() function which the function that will return html
	 * @param $views array of viewName-viewRendered key values; linking the name of the view with view's dumped data
	 * returned by render's view function
	 */
	abstract public function show( $views );
	
	
	/**
	 * the consturctor
	 */
	function __construct(){
		
		if( ! self::$_was_intialized ){
		
			if( defined("_K_PAGE_TITLE") )
				$this->setTitle( _K_PAGE_TITLE );
				
			if( defined("_K_HTML_ENCODING") )
				$this->setEncoding( _K_HTML_ENCODING );
				
			if( defined("_K_PAGE_CONTENT_LANGUAGE") )
				$this->setContentLanguages( _K_PAGE_CONTENT_LANGUAGE );
				
			if( defined("_K_PAGE_DESCRIPTION") )
				$this->setDescription( _K_PAGE_DESCRIPTION );
				
			if( defined("_K_PAGE_KEYWORDS") )
				$this->setKeywords( _K_PAGE_KEYWORDS );
			
			if( defined("_K_SHORTCUT_ICON") )
				$this->setShortcutIcon( _K_SHORTCUT_ICON );
			
			if( defined("_K_ICON") )
				$this->setIcon( _K_ICON );
				
			if( defined("_K_APPLE_TOUCH_ICON") )
				$this->setAppleTouchIcon( _K_APPLE_TOUCH_ICON );
			
			//$this->setExpires( date("r", strtotime("now +2 days")) );
			$this->setExpires( date("r", strtotime("now +1 second")) ); 
			
			if( @$GLOBALS['_K_CSS'] ){
			
				if( is_array($GLOBALS['_K_CSS']) )
					foreach( $GLOBALS['_K_CSS'] as $css )
						self::addCssPath($css);
				else
					self::addCssPath($GLOBALS['_K_CSS']);
				
			}
			
			
			if( @$GLOBALS['_K_JS'] ){
			
				if( is_array($GLOBALS['_K_JS']) )
					foreach( $GLOBALS['_K_JS'] as $css )
						self::addJScriptPath($css);
				else
					self::addJScriptPath($GLOBALS['_K_JS']);
				
			}
			
			self::$_was_intialized = true;
		}
	}
	
	
	
	
	/**
	 * adds the javascript to the html header
	 */
	public function addJScript( $jscript, $atBeginning=false  ){
		if(! $atBeginning)
			self::$jscript[] = $jscript;
		else
			self::$jscript = array_merge(array($jscript), self::$jscript);
	}
	
	/**
	 * adds the jquery to the html header
	 */
	public function addJQuery( $jquery, $atBeginning=false  ){
		if(! $atBeginning)
			self::$jquery[] = $jquery;
		else
			self::$jquery = array_merge(array($jquery), self::$jquery);
	}
	
	/**
	 * adds the css to the html header
	 */
	public function addCss( $css, $atBeginning=false  ){
		if(! $atBeginning)
			self::$css[] = $css;
		else
			self::$css = array_merge(array($css), self::$css);
	}
	
	/**
	 * adds the jsript file path to the html header
	 * @param string $path
	 * @param boolean $atBeginning whether to insert this path at the beginning of the array or not, default = false
	 */
	public function addJScriptPath( $path, $atBeginning=false ){
		if( ! in_array($path, self::$jscript_path) ){
			if(! $atBeginning)
				self::$jscript_path[] = $path;
			else
				self::$jscript_path = array_merge(array($path), self::$jscript_path);
		}
	}
	
	/**
	 * adds the css file path to the html header
	 */
	public function addCssPath( $css, $atBeginning=false  ){
		if( ! in_array($css, self::$css_path) ){
			if(! $atBeginning)
				self::$css_path[] = $css;
			else
				self::$css_path = array_merge(array($css), self::$css_path);
		}
	}
	
	
	
	/**
	 * sets the title in the html header
	 */
	public static function setTitle( $title ){
		self::$title = $title;
	}
	
	/**
	 * sets the encoding 
	 */
	public static function setEncoding( $encoding ){
		self::$encoding = $encoding;
	}
	
	/**
	 *  set the EXPIRES key in meta header
	 */
	public static function setExpires( $expires ){
		self::$EXPIRES = $expires;
	}
	
	/**
	 * add a key to CONTENT-LANGUAGE meta header
	 */
	public static function setContentLanguages( $cl ){
		self::$CONTENT_LANGUAGE = $cl;
	}
	
	/**
	 * add a key to description meta header
	 */
	public static function setDescription( $description ){
		self::$description = $description;
	}
	
	/**
	 * add a key to keywords meta header
	 */
	public static function setKeywords( $key ){
		self::$keywords = $key;
	}
	
	public static function setShortcutIcon( $si ){
		self::$SHORTCUT_ICON = $si;
	}
	
	public static function setIcon( $i ){
		self::$ICON = $i;
	}
	
	public static function setAppleTouchIcon( $ati ){
		self::$APPLE_TOUCH_ICON = $ati;
	}
	

	
	
	
	
	
	public function getTitle(){
		return self::$title;
	}
	public function getEncoding(){
		return self::$encoding;
	}
	public function getExpires(){
		return self::$EXPIRES;
	}
	public function getContentLanguages(){
		return self::$CONTENT_LANGUAGE;
	}
	public function getDescriptions(){
		return self::$description;
	}
	public function getKeywords(){
		return self::$keywords;
	}
	public function getShortcutIcon(){
		return self::$SHORTCUT_ICON;
	}
	public function getIcon(){
		return self::$ICON;
	}
	public function getAppleTouchIcon(){
		return self::$APPLE_TOUCH_ICON;
	}
	
	
	
	public function getJScripts(){
		return self::$jscript;
	}
	public function getJQueries(){
		return self::$jquery;
	}
	public function getCss(){
		return self::$css;
	}
	public function getJScriptPaths(){
		return self::$jscript_path;
	}
	public function getCssPaths(){
		return self::$css_path;
	}
	
	
	/**
	 * clears the css paths
	 */
	public function clearCssPaths(){
		self::$css_path = array();
	}
	
	
	
	
	public function dump(){
	
		print_r(
			array(
				"public" => $this,
				"private" => array(
					'title' => self::$title,
					'encoding' => self::$encoding,
					'CONTENT_LANGUAGE' => self::$CONTENT_LANGUAGE,
					'EXPIRES' => self::$EXPIRES,
					'description' => self::$description,
					'keywords' => self::$keywords,
					'SHORTCUT_ICON' => self::$SHORTCUT_ICON,
					'ICON' => self::$ICON,
					'APPLE_TOUCH_ICON' => self::$APPLE_TOUCH_ICON,
				)
			)
		);
	}
	
}


