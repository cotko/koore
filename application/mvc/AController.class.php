<?php

abstract class AController{
	
	private static $override_layout = false;
	
	private static $use_common_views = true;
	private static $use_common_controller_views = true;
	
	
	/**
	 * data got from runned action will be stored here by MVCHandler
	 * @var mixed
	 */
	public $data;
	
	/**
	 * this is the action which will be called by MVCHandler
	 * it is informational if controller needs to know which action will be run
	 * it will always be lowcase
	 * @var string
	 */
	public $action;
	
	/**
	 * must implement index() method which is default method
	 * that will be called if no other action is set via url
	 */
	abstract public function indexAction();
	
	/**
	 * should return false in order to use the MVC system (veiews, layouts..)
	 * or true in case if we just want to output the data returned by this controller (without wrapping it with html etc)
	 */
	public function suppressViews(){
		return false;
	}
	
	/**
	 * returns the type of data to be returned
	 * currently implemented: html, plain, json, javascript
	 */
	public function getDataType(){
		return "html";
	}
	
	/**
	 * sets the layout that should be used instead of default one
	 */
	protected function setLayout( $layout ){
		self::$override_layout = $layout;
	}
	
	/**
	 * see usesCommonViews()
	 * @param boolean $useThem
	 */
	protected function useCommonViews( $useThem ){
		self::$use_common_views = $useThem;
	}
	
	
	/**
	 * see usesCommonControllerViews() 
	 * @param boolean $useThem
	 */
	protected function useCommonControllerViews( $useThem ){
		self::$use_common_controller_views = $useThem;
	}
	
	
	/**
	 * structure for views<br />
	 * a) /views/{common_view1, common_view2...} these are views common for the all controllers<br />
	 * b) /views/controllerName/{common_view1, common_view2...} these are views common for the whole controller (all its actions)
	 * c) /views/controllerName/controllerAction/{view1, view2...} these views are specific to controllers action which is called
	 * if useCommonViews() returns true, views a) will also be used besides views in c) (and maybe b), see useCommonControllerViews() )
	 * @return true by default
	 */
	public function usesCommonViews(){
		return self::$use_common_views;
	}
	
	
	/**
	 * structure for views:<br />
	 * a) /views/{common_view1, common_view2...} these are views common for the all controllers<br />
	 * b) /views/controllerName/{common_view1, common_view2...} these are views common for the whole controller (all its actions)
	 * c) /views/controllerName/controllerAction/{view1, view2...} these views are specific to controllers action which is called
	 * if useCommonControllerViews() returns true, views b) will also be used besides views in c) (and maybe a), see useCommonViews() )
	 * @return true by default
	 */
	public function usesCommonControllerViews(){
		return self::$use_common_controller_views;
	}
	
	/**
	 * returns newly set layout or false if no layout was set in the controller 
	 */
	public function getLayout(){
		return self::$override_layout;
	}
	
	/**
	 * function which will be called before controller's action is called
	 * code that needs to be executed on every action can be put here instead of on beginning of every action
	 * 
	 * it can return something or nothing
	 * if this function returns something (not null) then no other methods will be called and only value returned by this function will be send back!
	 * (usefull for checking the login etc..)
	 */
	public function onBeforeControllerActionCall(){
	
	}
	
	
	/**
	 * function which will be called after controller's action is called
	 * you can for example alter output from runned action which was stored in controlelr's $data variable 
	 */
	public function onAfterControllerActionCall(){
	
	}
}

