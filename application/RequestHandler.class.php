<?php

/**
 * handles the request url
 * url can be like: .../lng?/controller/controller_action/key1/value1/key2/value2/key3/value3...
 * where lng is optional parameter, it's checked against the $availableLngs array of lanugages
 * if first parameter is found in that array it is considered as a language parameter, else as the controller's name..
 * other key-value parameters are expected to be in pairs
 */
class RequestHandler{
	
	private static $lng 				= "";
	
	private static $controller 			= "";
	
	private static $controller_action 	= "";
	
	private static $request_params;
	
	/**
	 * constructor
	 * @param $availableLngs the array of languages, should be passed as $available_languages array from Locale class
	 * @param $defaultControllerAndAction default controller if no controller set and its default action, array("controller"=>index, "action"=>index) is default
	 */
	public function __construct( $availableLngs=array(), $defaultControllerAndAction=array("controller"=>'index', "action"=>'index') ){
	
		self::$request_params = array();
	
		$this->init( $availableLngs, $defaultControllerAndAction );
		
	}
	
	/**
	 * parses the request params and sets some variables
	 * 
	 * @param string $availableLngs
	 * @param string $defaultControllerAndAction
	 * @param boolean $autodetectStandardParams default true: if last value has '?' at the end, it strips it and parses thode params
	 */
	private function init( $availableLngs, $defaultControllerAndAction,  $autodetectStandardParams=true ){
		$uri = trim($_SERVER['REQUEST_URI'], '/');
		$standardParams = '';
		if(strpos($uri, "?") !== false){
			$uri = explode("?", $uri);
			if(sizeof($uri) > 1)
				$standardParams = $uri[1];
			$uri = $uri[0];
		}
		$r 		= explode('/', $uri);
		$pos	= 0;
		$tmp_key= "";
		
		foreach( $r as $param ){
			//check for lanugage
			if( $pos == 0 && array_key_exists($param, $availableLngs) )
				self::$lng = $param;
			//this is controller's name
			else if( self::$controller == "" )
				self::$controller = $param;
			//this is controller's action
			else if( self::$controller_action == "" )
				self::$controller_action = $param;
			//this is param key
			else if( $tmp_key == "" )
				$tmp_key = urldecode($param);
			//this is param key's value
			else{
				self::$request_params[$tmp_key] = urldecode($param);
				$tmp_key = "";
			}
			$pos++;
		}
		
		if($autodetectStandardParams && $standardParams != '')
			$this->parseAndAddStandardParams($standardParams);
		
		if( self::$controller == "" )
			self::$controller = $defaultControllerAndAction['controller'];
			
		if( self::$controller_action == "" )
			self::$controller_action = $defaultControllerAndAction['action'];
		
	}
	
	private function parseAndAddStandardParams($standardParams){
		$p = explode('&', $standardParams);
		foreach($p as $paramValue){
			$paramValue = explode('=', $paramValue); 
			self::$request_params[$paramValue[0]] = $paramValue[1];
		}
	}
	
	
	/**
	 * returns the parameter set in get request
	 * if no param found for that key, it returns empty string
	 * @param $key for getting it's parameter
	 */
	public static function getParam( $key = '' ){
		if( array_key_exists($key, self::$request_params) )
			return self::$request_params[$key];
			
		else return "";
	}
	
	/**
	 * @returns boolean whether language is set via url or not
	 */
	public static function isLanguageSet() {
		return self::getLng() != '';
	}
	
	/*
	 * these getters are only to be used by MVCHandler and its' logics
	 */
	public static function getControllerName(){
		return strtolower(self::$controller);
	}
	public static function getActionName(){
		return strtolower(self::$controller_action);
	}
	public static function getLng(){
		return self::$lng;
	}
	
	public function dump(){
		print_r( array(
			'$lng' => self::$lng,
			'$controller' => self::$controller,
			'$controller_action' => self::$controller_action,
			'$request_params' => self::$request_params
		));
	}
	
}
