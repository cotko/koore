<?php


/**
 * helper ..
 * @author mitja
 *
 */
class Session{
	

	private $timeout;
	
	private $regenerationTime = 300; //regenerate session id every 5 minutes
	
	private static $staticReference; //static reference to $this, set in constructor
	
	/**
	 * new session object
	 * @param int $timeout seconds for expiration (>=60), default is 900 (=15 min)
	 */
	public function __construct($timeout=900){
		$this->init($timeout);
		
		self::$staticReference = $this;
		
	}
	
	
	private function init($timeout){
		$this->timeout = $timeout;
		
		session_cache_expire( round($timeout / 60) );
		ini_set("session.gc_maxlifetime", $timeout );
		ini_set("session.cookie_lifetime", $timeout );
		
		session_start();
		$this->checkExpired();
		
	}
	
	
	private function checkExpired(){
		$time = time();
		
		//expire session
		if( @$_SESSION['koore_session_lastactivity'] && ($time - $_SESSION['koore_session_lastactivity'] > $this->timeout) ) {
			self::clearAll();
		}
		//regenerate session id to make hacking more difficult
		if( ! @$_SESSION['koore_session_time_created'] )
			$_SESSION['koore_session_time_created'] = $time;
			
		else if( $time - $_SESSION['koore_session_time_created'] > $this->regenerationTime ){
			session_regenerate_id( true );
			$_SESSION['koore_session_time_created'] = $time;
		} 
			
		$_SESSION['koore_session_lastactivity'] = $time;
		
	}
	
	
	public static function put($key, $value){
		$_SESSION[$key] = $value;
	}
	
	
	public static function get($key){
		if( @$_SESSION[$key] )
			return $_SESSION[$key];
			
		else return "";
	}
	
	/**
	 * short-hand jQuery style function for put/get
	 */
	public static function val($key,$value=''){
		if($value==='')
			return self::get($key);
		self::put($key,$value);
	}
	
	
	public static function clear($key){
		if( @$_SESSION[$key] ){
			unset($_SESSION[$key]);
		}
		
	}
	
	public static function getTimeoutSeconds(){
		return self::$staticReference->timeout;
	}
	
	/**
	 * destroys teh session
	 */
	public static function clearAll(){
		@session_unset();
		@session_destroy();
		$_SESSION = array();
	}
	
	
	
	
	
}