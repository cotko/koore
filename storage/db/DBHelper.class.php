<?php


class DBHelper{
	
	
	
	private static $db;
	
	private static $_CONNECTION_OPENED = false;
	
	public function __construct($dbhost, $dbuser, $dbpass, $dbname){
		
		self::$db = new DB( $dbhost, $dbuser, $dbpass, $dbname);
		
	}
	
	/**
	 * fetch query
	 * @param $query
	 * @param $fieldForId is field in query that will be used instead of default incrementing key value 
	 *			(should be unique else it will override other rows with the same field value!)
	 */
	public static function fetch($query, $fieldForId=false){
		
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		
		$result = false;
		
		try{
			$result = self::$db->fetch( $query, $fieldForId );
		}catch(Exception $e){
			Debugger::addException( $e );
			$result = false;
		}
		
		return $result;
	}
	
	/**
	 * same as fetch, but returns current() from results array
	 * should be used with limit 1 or so..
	 * @param string $query
	 * @param int optional $fieldForId
	 */
	public static function fetchOne($query, $fieldForId=false){
		
		
		$result = self::fetch( $query, $fieldForId );
		
		if( is_array($result) && sizeof($result) > 0 )
			return current( $result );
		
		return false;
	}
	
	
	
	/**
	 * inserts the values into a table, returns true/false
	 * @param string $table
	 * @param array $data
	 * @return mixed: boolean true or array explaining the error
	 */
	public static function insert( $table, $data ){
		
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		
		try{
			$result = self::$db->insert( $table, $data );
		}catch(Exception $e){
			Debugger::addException( $e );
			return array("error"=>$e, "error_nr"=>mysql_errno());
		}
		return true;
	}
	
	/**
	 * inserts the values into a table, returns last insert id
	 * @param string $table
	 * @param array $data
	 * @return last insert id or -1 on error
	 */
	public static function insertOne( $table, $data ){
		
		if(self::insert($table, $data))
			return self::$db->getLastInsertId();
		
		return -1;
	}
	
	
	/**
	 * updates the data in table
	 * returns the number of affected rows or false on error
	 * @param string $table
	 * @param array $data
	 * @param string optional $where
	 * @return mixed: boolean true or array explaining the error
	 */
	public static function update( $table, $data, $where="1=1" ){
		
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		
		try{
			$nraffectedrows = self::$db->update( $table, $data, $where );
		}catch(Exception $e){
			Debugger::addException( $e );
			return array("error"=>$e, "error_nr"=>mysql_errno());
		}
		return $nraffectedrows;
	}
	
	/**
	 * deletes records from table
	 * @param string $table
	 * @param array $data key value pairs for WHERE statement
	 *
	 */
	public static function delete( $table, $data ){
		
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		
		try{
			$nraffectedrows = self::$db->delete( $table, $data );
		}catch(Exception $e){
			Debugger::addException( $e );
			return array("error"=>$e, "error_nr"=>mysql_errno());
		}
		return $nraffectedrows;
	}
	
	/**
	 * executes some query
	 * @param String $query
	 * @return mixed: boolean true or array explaining the error
	 */
	public static function query($query){
		return self::query_execute($query);
	}
	public static function query_execute($query){
		
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		
		try{
			self::$db->query_execute($query);
		}catch(Exception $e){
			Debugger::addException( $e );
			return array("error"=>$e, "error_nr"=>mysql_errno());
		}
		
		return true;
	}
	
	
	public static function escape($string){
		if( !self::$_CONNECTION_OPENED ){
			self::connect();
			self::$_CONNECTION_OPENED = true;
		}
		return self::$db->escape($string);
	}
	
	
	/**
	 * builds a query 
	 * example: <b>buildQuery(select * from user where name={name} and surname={surname} and age>{min_age}, array("name"=>"Was", "surname"=>"istdas", "min_age"=>15))</b> 
	 * @param string $query
	 * @param array $data
	 */
	public static function buildQuery( $query, $data ){
		if( ! is_array($data) )
			return $query;
			
		foreach( $data as $k=>$v ){
			if( is_integer($v) )
				$query = str_replace('{'.$k.'}', $v, $query);
			else
				$query = str_replace('{'.$k.'}', "'".self::$db->escape($v)."'", $query);
		}
		return $query;
	}
	
	
	
	public function openConnection(){
		self::connect();
	}
	
	
	public function closeConnection(){
		
		try{
			self::$db->disconnect();
		}catch(Exception $e){
			Debugger::addException( $e );
		}
	}
	
	
	
	private static function connect(){
		try{
			self::$db->connect();
		}catch(Exception $e){
			Debugger::addException( $e );
		}
	} 
	
}