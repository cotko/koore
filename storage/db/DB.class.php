<?php

class DB {


	private $dbhost;
	private $dbuser;
	private $dbpass;
	private $dbname;
	private $persistant;
	private $charset;
	
	private $mysql;
	private $db;
	
	private $connecitonOpened = false;
	
	private $lastInsertId;
	
	
	public function __construct($dbhost, $dbuser, $dbpass, $dbname, $persistant = true, $charset = 'utf8') {
		$this->dbhost = $dbhost;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbname = $dbname;
		$this->persistant = $persistant;
		$this->charset = $charset;
	}
	
	
	public function connect(){
		$prefix = $this->persistant ? 'p:' : '';
		$this->mysql = new mysqli($prefix . $this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
		if ($this->mysql->connect_errno)
			throw new Exception("db error: could not connect to: '$this->dbhost': (errno: " . $mysqli->connect_errno . ") " . $mysqli->connect_error);
    	
    	$this->mysql->set_charset($this->charset);
    	
		$this->connecitonOpened = true;
		
		return true;
	}
	
	
	public function disconnect(){
		if( ! $this->connecitonOpened )
			return true;
		
		
		$this->mysql->close();
			
		return true;
	}
	
	private function getErrorDescription() {
		return "(errno: " . $this->mysql->errno . ") " . $this->mysql->error;
	}
	
	public function getLastInsertId() {
		return $this->lastInsertId;
	}
	
	public function insert( $table, $data ){
		$table = trim($table, '`');
		
		$query = "INSERT INTO `$table` ";
		
		$keys = "";
		$values = "";
		foreach( $data as $k=>$v ){
			$keys .= "`" .$k . "`, ";
			$values .= "'" . $this->escape($v) . "', ";
		}
		$keys = "(" . rtrim($keys, ", ") . ")";
		$values = " VALUES(". rtrim($values, ", ") .")";
		
		$query .= $keys . $values . ";";
		
		if( $this->mysql->query($query) ) {
			$this->lastInsertId = $this->mysql->insert_id;
			return true;
		} else {
			throw new Exception("db error: unable to insert: " . $this->getErrorDescription());
		}
	}
	
	
	public function update( $table, $data, $where="1=1" ){
		$table = trim($table, '`');
		
		$query = "UPDATE `$table` SET ";
		
		foreach( $data as $k=>$v )
			$query .= "`$k` = '". $this->escape($v) ."', ";
			
		$query = rtrim($query, ", ") . " WHERE " . $where . ";";
		//echo $query;
		if( $this->mysql->query($query) ) {
			return $this->mysql->affected_rows;
		} else {
			throw new Exception("db error: unable to update: " . $this->getErrorDescription());
		}
		
	}
	
	public function delete( $table, $data ){
		$table = trim($table, '`');
		
		$query = "DELETE FROM `$table` ";
		
		$data2 = array();
		foreach( $data as $k=>$v )
			$data2[] = "`$k` = '". $this->escape($v) ."'";
			
		$where = join(" AND ", $data2);
		$query .=  " WHERE " . $where . ";";
		
		if( $this->mysql->query($query) ) {
			return $this->mysql->affected_rows;
		} else {
			throw new Exception("db error: unable to delete: " . $this->getErrorDescription());
		}
	}
	
	
	/**
	 * fetch query
	 * @param $query
	 * @param $fieldForId is field in query that will be used instead of default incrementing value 
	 *			(should be unique else it will override other rows with the same field value!)
	 */
	public function fetch($query, $fieldForId=false){
		
		$sql = $this->mysql->query($query);
		if( ! $sql )
			throw new Exception("db error: unable to fetch: " . $this->getErrorDescription());
		
		$ret = array();
		$i = 0;
		if ($result = $this->mysql->query($query)) {
		    while ($row = $result->fetch_assoc()) {
		        $ret[ $fieldForId && isset($row[$fieldForId]) ? $row[$fieldForId] : $i++] = $row;
		    }
		    $result->free();
		}
		
		return $ret;
	}
	
	
	public function query_execute($query){
		
		
		if( ! $this->mysql->query($query) )
			throw new Exception("db error: unable to execute: " . $this->getErrorDescription());
		
		
		return true;
	}
	
	
	public function escape( $string ) {
		return $this->mysql->real_escape_string($string);
	}
	
	
}