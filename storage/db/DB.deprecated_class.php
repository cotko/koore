<?php

/**
 *
 * !!!! USES mysql_ which is deprecated, insead mysqli_ should be used !!!!
 *
 */
class DB_deprecated{


	private $dbhost;
	private $dbuser;
	private $dbpass;
	private $dbname;
	
	private $mysql;
	private $db;
	
	private $connecitonOpened = false;
	
	public function __construct($dbhost, $dbuser, $dbpass, $dbname) {
		$this->dbhost = $dbhost;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dbname = $dbname;
	}
	
	
	public function connect(){
		
		$this->mysql = @mysql_connect($this->dbhost,$this->dbuser,$this->dbpass);
		
		if( ! $this->mysql )
			throw new Exception("db error: could not connect to: '$this->dbhost'");
		
		
		$this->db = @mysql_select_db($this->dbname,$this->mysql);
		if( ! $this->db )
			throw new Exception("db error: could not select the db: '$this->dbname' " . mysql_error($this->mysql));
		
		
		$this->connecitonOpened = true;
		
		return true;
	}
	
	
	public function disconnect(){
		if( ! $this->connecitonOpened )
			return true;
			
		if( ! @mysql_close($this->mysql) )
			throw new Exception("db error: unable to close the connection");
		
		
		return true;
	}
	
	
	public function insert( $table, $data ){
		$table = trim($table, '`');
		$this->errordesc = "";
		
		$query = "INSERT INTO `$table` ";
		
		$keys = "";
		$values = "";
		foreach( $data as $k=>$v ){
			$keys .= $k . ", ";
			$values .= "'" . $this->escape($v) . "', ";
		}
		$keys = "(" . rtrim($keys, ", ") . ")";
		$values = " VALUES(". rtrim($values, ", ") .")";
		
		$query .= $keys . $values . ";";
		
		if( @mysql_query($query, $this->mysql) )
			return true;
		else
			throw new Exception("db error: unable to insert: '".mysql_error($this->mysql)."'");
		
		
	}
	
	
	public function update( $table, $data, $where="1=1" ){
		$table = trim($table, '`');
		$this->errordesc = "";
		
		$query = "UPDATE `$table` SET ";
		
		foreach( $data as $k=>$v )
			$query .= "`$k` = '". $this->escape($v) ."', ";
			
		$query = rtrim($query, ", ") . " WHERE " . $where . ";";
		//echo $query;
		if( @mysql_query($query, $this->mysql) ){
			//return true;
			return mysql_affected_rows($this->mysql);
		}
		else
			throw new Exception("db error: unable to update: '".mysql_error($this->mysql)."'");
		
		
	}
	
	public function delete( $table, $data ){
		$table = trim($table, '`');
		$this->errordesc = "";
		
		$query = "DELETE FROM `$table` ";
		
		$data2 = array();
		foreach( $data as $k=>$v )
			$data2[] = "`$k` = '". $this->escape($v) ."'";
			
		$where = join(" AND ", $data2);
		$query .=  " WHERE " . $where . ";";
		
		if( @mysql_query($query, $this->mysql) ){
			//return true;
			return mysql_affected_rows($this->mysql);
		}
		else
			throw new Exception("db error: unable to delete: '".mysql_error($this->mysql)."'");
		
		
	}
	
	
	/**
	 * fetch query
	 * @param $query
	 * @param $fieldForId is field in query that will be used instead of default incrementing value 
	 *			(should be unique else it will override other rows with the same field value!)
	 */
	public function fetch($query, $fieldForId=false){
		$this->errordesc = "";
		$sql = mysql_query($query, $this->mysql);
		if( ! $sql )
			throw new Exception("db error: unable to fetch: '".mysql_error($this->mysql)."'");
		
		
		$ret = array();
		$i = 0;
		while( $row = mysql_fetch_array($sql, MYSQL_ASSOC) )
			$ret[ $fieldForId && isset($row[$fieldForId]) ? $row[$fieldForId] : $i++] = $row;
		
		return $ret;
		
	}
	
	
	public function query_execute($query){
		$this->errordesc = "";
		
		if( ! @mysql_query($query, $this->mysql) )
			throw new Exception("db error: unable to execute: '".mysql_error($this->mysql)."'");
		
		
		return true;
	}
	
	
	public function escape( $string ) {
		if(get_magic_quotes_runtime())
			$string = stripslashes( $string );
		return @mysql_real_escape_string( $string, $this->mysql );
	}
	
	
}