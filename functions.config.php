<?php

//various usefull functions


function print_rr($a){
	echo '<pre>'.print_r($a, true).'</pre>';
}

/**
 * executes php file and returns the content
 * @param $filename
 * @param $params optional array of params which will be accessible to php script file via $params variable
 */
function php_get_contents($filename, $params=array()){
	ob_start();
	include_once( $filename );
	$ret = ob_get_contents();
	ob_end_clean();
	return $ret;
}


function array_sort($array, $on, $order='ASC'){
	$new_array = array();
	$sortable_array = array();
	if (count($array) > 0) {
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				foreach ($v as $k2 => $v2) {
					if ($k2 == $on) {
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}
		switch(strtolower($order)) {
			case 'sort_asc':
			case 'asc':
				asort($sortable_array);
				break;
			case 'sord_desc':
			case "desc":
				arsort($sortable_array);
				break;
		}
		foreach($sortable_array as $k => $v) {
			$new_array[] = $array[$k];
		}
	}
	return $new_array;
}