<?php
 
/**
 * static class for debugging
 */
class Debugger{
	
	
	private static $errors = array();
	
	
	
	public static function addError( $error ){
		
		self::$errors[] = $error;
		
	}
	
	public static function addException( $exception ){
		
		if ($exception instanceof ErrorException)
			self::$errors[] = $exception->getMessage() . ", file: " . $exception->getFile() . ",  at line: " . $exception->getLine();
		else
			self::$errors[] = $exception->getMessage();
		
	}
	
	public static function printErrors(){
		
		print_r(self::$errors);
		
	}
	
	
	public static function getErrors(){
		
		return self::$errors;
		
	}
	
	
	public static function getLastEror(){
		return end(self::$errors);
	}
	
}
