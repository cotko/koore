<?php

Logger::init();

class Logger{
	
	private static $logDir = false;
	private static $inited = false;
	
	private static $defaultLog = 'logs';
	private static $currentLog = 'logs';
	
	public static function init($logDir=false){
		if($logDir!==false && ! is_writable($logDir)){
			Debugger::addError(get_class() . "->init(): directory not writable: '$logDir'");
			return false;
		}
		else if($logDir !== false){
			self::$logDir = $logDir;
		}
		else{
			self::$logDir = dirname(__FILE__) . "/logs/";
			if(! is_writable(self::$logDir)){
				//maybe it is in cache, go out and into log folder
				self::$logDir = dirname(__FILE__) . "/../../debugging/logs/";
			}
			if(! is_writable(self::$logDir)){
				Debugger::addError(get_class() . "->init(): directory not writable: '".self::$logDir."'");
				self::$logDir = false;
				return false;
			}
		}
		self::$inited = true;
	}
	
	public static function setLogFile($logFile){
		self::$currentLog = $logFile;
	}
	
	public static function log($msg, $timestamp=true, $prepend="", $useThisLogFile=false){
		$logFile = self::$currentLog;
		if($useThisLogFile !== false)
			$logFile = $useThisLogFile;
		if(!self::$inited)
			return false;
		$line = $prepend . ($timestamp ? date('d.m.Y H:i') . ' * ' : ' -------------- ') . $msg . "\n";
		file_put_contents(self::$logDir.$logFile, $line, FILE_APPEND);
	}
	
}

