<?php

//init this dir if globaly set
if(defined("THUMBER_THUMBS_DIR")){
	Thumber::setThumbsDirectory(THUMBER_THUMBS_DIR);
}
	

//no includer needed, this is just a simple class..
class Thumber{
	
	private static $thumbsLocation;
	
	private static function checkDirectory($dir){
		//must be writable!
		if(!is_dir($dir))
			Debugger::addError(get_called_class() . " -> this is not directotry: '".$dir."'");
		else if(!is_writable($dir))
			Debugger::addError(get_called_class() . " -> directotry is not writable: '".$dir."'");
		else 
			return true;
		return false;
	}
	
	public static function setThumbsDirectory($dir){
		if(self::checkDirectory($dir))
			self::$thumbsLocation = rtrim($dir, '/');
	}
	
	public static function getThumb($photo, $w, $h, $cropToCenter=true){
		if(!self::$thumbsLocation){
			Debugger::addError(get_called_class() . " getThumb()-> no tumb directory set, avoiding making of thumbnail!");
			return '';
		}
		//check: first remove slashes, if file doesnt exist, add a slash at the beginning
		$orig_photo = $photo;
		$photo = trim($photo, '/');
		if(!is_file($photo)){
			$photo = "/$photo";
			if(!is_file($photo)){
				Debugger::addError(get_called_class() . " getThumb() not a file: '$orig_photo'");
				return '';
			}
		}
		$name = basename($photo) . "_".$w."x".$h.".png";
		//already exists?
		if(is_file(self::$thumbsLocation.'/'.$name)){
			return self::$thumbsLocation.'/'.$name;
			}
		
		if(self::resizeAndCropIt($photo, self::$thumbsLocation, $name, $w, $h, $cropToCenter))
			return self::$thumbsLocation.'/'.$name;
		else return self::$thumbsLocation . "/nothumb.png";
	}
	
	public static function resizeAndCropIt($src_file, $dest_dir, $dest_name, $w, $h, $cropToCenter=true){
		if(!self::$thumbsLocation){
			Debugger::addError(get_called_class() . " resizeAndCropIt()-> no tumb directory set, avoiding making of thumbnail!");
			return false;
		}
		$success = true;
		//get image
		$info = getimagesize($src_file);
		$orig_width = $info[0];
		$orig_height = $info[1];
		$mime = $info['mime'];
		
		$src = '';
		switch ($mime) {
		case 'image/jpeg':
			$src = @imagecreatefromjpeg($src_file);
			break;
		case 'image/gif':
			$src =  @imagecreatefromgif($src_file);
			break;
		case 'image/png':
			$src = @imagecreatefrompng($src_file);
			break;
		}
	
		$t_w = 0;
		$t_h = 0;
	
		$m_w = min($w, $orig_width);
		$m_h = min($h, $orig_height);
	
		if( $orig_width >= $orig_height){
			//we know width will be larger than height..
			$t_w = $w;
			$t_h = ($w*$orig_height) / $orig_width;
		}
		else{
			$t_h = $h;
			$t_w = ($h*$orig_width) / $orig_height;
		}
	
		//repair
		if( $t_h < $h ){
			$t_h = $h;
			$t_w = ($h*$orig_width) / $orig_height;
		}
		else if( $t_w < $w ){
			$t_w = $w;
			$t_h = ($w*$orig_height) / $orig_width;
		}
	
		$filename = $dest_dir . '/' . $dest_name;
	
		$tmp = @imagecreatetruecolor($t_w,$t_h);
	
	
		// resize image
		$success = @imagecopyresampled($tmp,$src,0,0,0,0,$t_w,$t_h,$orig_width,$orig_height);
		if( $success ){
		
			//now crop it if needed
			if( $t_w != $w || $t_h != $h ){
				
				if( $cropToCenter ){
					$src_x = $t_w <= $w ? 0 : (($t_w-$w)/2);
					$src_y = $t_h <= $h ? 0 : (($t_h-$h)/2);
				}
				else{
					$src_x = $t_w <= $w ? 0 : (($t_w-$w)/2);
					$src_y = 0;
				}
				
				//*
				$tmp2 = @imagecreatetruecolor($w,$h);
				$success2 = @imagecopyresampled($tmp2,$tmp,0,0,$src_x,$src_y,$w,$h,$w,$h);
				@imagedestroy($tmp);
				/*/
				$success2 = true;
				$tmp2 = &$tmp;
				//*/
				
				if( $success2 ){

					if( ! @imagejpeg($tmp2,$filename) ) {
						Debugger::addError(" ..".get_called_class() . " resizeAndCropIt() - 3 $src_file problem!");
						$success = false;
					} 
					
					@imagedestroy($src);
					@imagedestroy($tmp2);
				}
				else{
					Debugger::addError(" ..".get_called_class() . " resizeAndCropIt() - 2 $src_file problem!");
					$success = false;
				}
			}
			//or just save it
			else{
				
				if( ! @imagejpeg($tmp,$filename) ) {
					Debugger::addError(" ..".get_called_class() . " resizeAndCropIt() - 4 $src_file problem!");
					$success = false;
				}
				
				@imagedestroy($tmp);
			}
			
		}
		else{
			Debugger::addError(" ..".get_called_class() . " resizeAndCropIt() - $src_file problem!");
			$success = false;
		}
			
		return $success;
	}
	
}