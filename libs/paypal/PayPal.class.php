<?php
$_USE_SANDBOX = false;


$apisettings = array(
	"sandbox" => array(
		"apiuser" => "mitja_1295965243_biz_api1.yahoo.com",
		"apipass" => "1295965252",
		"apisig" => "AvRb86d-QFhtI7sjJkWdM6DcUPjTALgztbx-hZSGWAWHLwyDRrc9ulCT",
		"appid" => "APP-80W284485P519543T", //https://www.x.com/message/194604#194604
		"endpoint" => "https://svcs.sandbox.paypal.com/AdaptivePayments/Pay",
		"posformturl" => "https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay",
		"receiverEmail" => "mitja_1295965243_biz@yahoo.com"
	),
	"live" => array(
		"apiuser" => "contact_api1.studentbrakeaway.com",
		"apipass" => "JMEC7A9JDMRC5U2G",
		"apisig" => "AHhOxgeB3uCC.ctKs4bGJ-mfd63OATF1i93GxBnQPIFFObTRwHoQYLRf",
		"appid" => "APP-5BX023374H6785419",
		"endpoint" => "https://svcs.paypal.com/AdaptivePayments/Pay",
		"posformturl" => "https://paypal.com/webapps/adaptivepayment/flow/pay",
		"receiverEmail" => "contact@studentbrakeaway.com"
	)
);

$settings = $_USE_SANDBOX ? $apisettings["sandbox"] : $apisettings["live"];


PayPal::setup(
$settings["apiuser"],
$settings["apipass"],
$settings["apisig"],
$settings["appid"]
);
PayPal::$endpoint = $settings["endpoint"];
PayPal::$payUrl = $settings["posformturl"];
PayPal::$receiverEmail = $settings["receiverEmail"];
//PayPal::$currencyCode = Commons::getCurrencyCode();
//PayPal::$languageCode = "en_US";

/*
 // Report all PHP errors
 error_reporting(1);

 // Same as error_reporting(E_ALL);
 ini_set('error_reporting', E_ALL);
 //*/

class PayPal{


	public static $actionType = "PAY";
	public static $receiverEmail = "mitja_1295965243_biz@yahoo.com";
	public static $currencyCode = "EUR";
	public static $languageCode = "sl_SI";
	public static $cancelUrl = "http://www.paypal.com";

	//*
	public static $payUrl = "https://paypal.com/webapps/adaptivepayment/flow/pay";
	public static $endpoint = "https://svcs.paypal.com/AdaptivePayments/Pay";
	/*/
	 public static $payUrl = "https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay";
	 public static $endpoint = "https://svcs.sandbox.paypal.com/AdaptivePayments/Pay";
	 //*/

	public static $ipnURL = "";

	public static $headers = null;

	public static function getFormURL(){
		return self::$payUrl;
	}

	public static function setup($apiUser, $apiPass, $apiSig, $appId, $requestFormat="NV", $responseFormat="NV"){

		self::$headers = array(
			"Content-Type: text/namevalue", // either text/namevalue or text/xml
			"X-PAYPAL-SECURITY-USERID: $apiUser",//API user
			"X-PAYPAL-SECURITY-PASSWORD: $apiPass",//API PWD
			"X-PAYPAL-SECURITY-SIGNATURE: $apiSig",//API Sig
			"X-PAYPAL-APPLICATION-ID: $appId",//APP ID
			"X-PAYPAL-REQUEST-DATA-FORMAT: $requestFormat",//Set Name Value Request Format
			"X-PAYPAL-RESPONSE-DATA-FORMAT: $responseFormat",//Set Name Value Response Format
		);

	}



	public static function getPayKey($amount, $returnUrl, $cancelUrl, $description="", $currencyCode="", $languageCode="", $receiverEmail="", $actionType=""){
		$currencyCode = $currencyCode == '' ? self::$currencyCode : $currencyCode;
		$receiverEmail = $receiverEmail == '' ? self::$receiverEmail : $receiverEmail;
		$actionType = $actionType == '' ? self::$actionType : $actionType;
		$cancelUrl = $cancelUrl == '' ? self::$cancelUrl : $cancelUrl;
		$currencyCode = $currencyCode == '' ? self::$currencyCode : $currencyCode;
		$languageCode = $languageCode == '' ? self::$languageCode : $languageCode;
		$description = $description == '' ? '' : "&memo=" . urlencode($description);
		//echo $description;
		$ipnUrl = self::$ipnURL == "" ? '' : "&ipnNotificationUrl=" . urlencode(self::$ipnURL);

		$requestUrl = 	"actionType=$actionType"
						."&returnUrl=" . urlencode($returnUrl)
						."&cancelUrl=" . urlencode($cancelUrl)
						."&currencyCode=$currencyCode"
						."&requestEnvelope.errorLanguage=$languageCode"
						."&receiverList.receiver(0).amount=$amount"
						."&receiverList.receiver(0).email=$receiverEmail"
						.$description
						.$ipnUrl;
		//."&trackingId=". md5(rand());
		//."&senderEmail=mitja_cotic@yahoo.com"; //tega ne posliji zravn ker bo pisal da je bla transakcija ze narjena
		//echo $requestUrl;
		$response = self::_parseAPIResponse(self::_getApiResponse($requestUrl));

		return @$response['payKey'] ? $response['payKey'] : null;
	}

	private static function _getApiResponse($apiString){
		//print_r(self::$headers);
		// setting the curl parameters.
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, self::$endpoint);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, self::$headers);
		//curl_setopt($ch, CURLOPT_HEADER, 1); // tells curl to include headers in response, use for testing
		// turning off the server and peer verification(TrustManager Concept).
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		// setting the NVP $my_api_str as POST FIELD to curl
		curl_setopt($ch, CURLOPT_POSTFIELDS, $apiString);
		// getting response from server
		$httpResponse = curl_exec($ch);
		//print_R($httpResponse);
		//
		if(!$httpResponse){
			//print_R($httpResponse);
			$response = "$apiString failed: ".curl_error($ch)."(".curl_errno($ch).")";
			return $response;
		}
		return $httpResponse;
	}

	private static function _parseAPIResponse($response){
		if(!is_array($response))
		$response = explode("&", $response);
		$parsed_response = array();
		foreach ($response as $i => $value){
			$tmpAr = explode("=", $value);
			if(sizeof($tmpAr) > 1) {
				$parsed_response[$tmpAr[0]] = $tmpAr[1];
			}
		}
		return $parsed_response;
	}
}




PayPayIPNandButton::$isSandbox = $_USE_SANDBOX;
PayPayIPNandButton::setDataSource($_POST);
class PayPayIPNandButton{

	const OK = 0;
	const ERR_IPN_INVALID = 1;
	const ERR_WRONG_RECEIVER_EMAIL = 2;
	const ERR_NOT_COMPLETED = 3;
	//additional - from db data
	const ERR_NO_SUCH_BUTTON_ID = 4;
	const ERR_DUPLICATE_TXN_ID = 5;
	const ERR_WRONG_PRICE = 6;
	//other
	const ERR_VALIDATION_TRY = -1;

	//ipn data, default in $_POST
	private static $_IPN_DATA = array();

	public static $isSandbox = false;

	private static $urlCheck = array(
		"sandbox" => "http://www.sandbox.paypal.com/cgi-bin/webscr",
		"live" => "https://www.paypal.com/cgi-bin/webscr"
	);

	private static function getValidateURL(){
		return self::$urlCheck[self::$isSandbox ? "sandbox" : "live"];
	}

	/**
	 * ipn data source, default should be $_POST
	 * @param array $data
	 */
	public static function setDataSource($data){
		self::$_IPN_DATA = $data;
	}

	/**
	 * standard check
	 * @param unknown_type $receiverEmail
	 * @param unknown_type $onylCompleted
	 */
	public static function checkIPN($receiverEmail="", $onylCompleted=true){
		if(self::$_IPN_DATA["receiver_email"] != $receiverEmail)
			return;
		if(self::$_IPN_DATA["receiver_email"] != $receiverEmail)
			return self::ERR_WRONG_RECEIVER_EMAIL;
		//check if completed
		if($onylCompleted && strtolower(self::$_IPN_DATA["payment_status"]) != "completed")
			return self::ERR_NOT_COMPLETED;
		//check ipn validity
		$ret = self::OK;
		$url = self::getValidateURL();
		$postdata = 'cmd=_notify-validate';
		foreach(self::$_IPN_DATA as $key => $value){
			$value = urlencode(stripslashes($value));
			$postdata .= "&$key=$value";
		}
		$web = parse_url($url);
		if($web['scheme'] == 'https') {
			$web['port'] = 443;
			$ssl = 'ssl://';
		}
		else{
			$web['port'] = 80;
			$ssl = '';
		}
		$fp = @fsockopen($ssl.$web['host'], $web['port'], $errnum, $errstr, 30);
		if(!$fp){
			$ret = self::ERR_VALIDATION_TRY;
		}
		else{
			$info = '';
			fputs($fp, "POST ".$web['path']." HTTP/1.1\r\n");
			fputs($fp, "Host: ".$web['host']."\r\n");
			fputs($fp, "Content-type: application/x-www-form-urlencoded\r\n");
			fputs($fp, "Content-length: ".strlen($postdata)."\r\n");
			fputs($fp, "Connection: close\r\n\r\n");
			fputs($fp, $postdata . "\r\n\r\n");
			while(!feof($fp)){
				$info .= @fgets($fp, 1024);
			}
			fclose($fp);
			//echo($info);
			if(eregi('VERIFIED', $info)){
				//ok
			}
			else{
				$ret = self::ERR_IPN_INVALID;
			}
		}
		return $ret;
	}

	/**
	 * custom database data checks..
	 */
	public static function additionalIPNcheck(){
		//check if this txn_id is already in database
		if(sizeof(DBHelper::fetch("select id_paypal_ipn from rapid_paypal_ipn where txn_id='".DBHelper::escape(self::$_IPN_DATA["txn_id"])."'")) > 0)
			return self::ERR_DUPLICATE_TXN_ID;
		//check button id
		$button = self::getCorrespondingRapidRapids();
		if(!is_array($button))
			return $button;
		//check price
		if($button["price"] != self::$_IPN_DATA["mc_gross"])
			return self::ERR_WRONG_PRICE;
		else
			return self::OK;
	}
	
	public static function getCorrespondingRapidRapids(){
		$f = explode(":", self::$_IPN_DATA["custom"]);//leave :, although :: is possible, we only need var at 0 index..
		$d = DBHelper::fetch("select * from rapid_rapids");
		foreach($d as $button){
			if(md5($button["paypal_button_id"]."_lala") == @$f[0]){
				return $button;
			}
		}
		return self::ERR_NO_SUCH_BUTTON_ID;
	}

	public static function getUserId(){
		$f = explode(":", self::$_IPN_DATA["custom"]);
		return empty($f[1]) ? -1 : intval($f[1]);
	}
	
	public static function getTEMPUserId(){
		$f = explode("::", self::$_IPN_DATA["custom"]);
		return empty($f[1]) ? -1 : intval($f[1]);
	}
	
	public static function isTEMPUserId(){
		return substr_count(self::$_IPN_DATA["custom"], '::') > 0;
	}
	
	public static function getPayPalHTMLButtons($idUser, $returnURL="", $cancelURL="",$useUserTMPTable=false){
		$d = DBHelper::fetch("select * from rapid_rapids order by rapids asc");
		//if valid user, false/null is send if user not logged in or so..
		if($idUser){
			$IPNResponseURL = "http://$_SERVER[HTTP_HOST]/rapidservice/ipn/";
			$additionalParams = '<input type="hidden" name="notify_url" value="'.$IPNResponseURL.'">' . "\n";
			if($returnURL!="")
				$additionalParams .= '<input type="hidden" name="return" value="'.$returnURL.'">' . "\n";
			if($returnURL!="")
				$additionalParams .= '<input type="hidden" name="cancel_return" value="'.$cancelURL.'">' . "\n";
			$buttons = array();
			foreach($d as &$button){
				$tmpParams = $additionalParams.
								'<input type="hidden" name="custom" value="'.md5($button["paypal_button_id"]."_lala").($useUserTMPTable ? '::' : ':').$idUser.'" />
								<input type="hidden" name="item_name" value="'.$button["rapids"]." ".Lng::translate("locale_paypal_buy_rapids_subject").'" />
								</form>';
				$button["paypal_button_html_modified"] = preg_replace('/(<\s*\/\s*form\s*>)/i', $tmpParams, $button["paypal_button_html"]);
			}
		}
		else{
			foreach($d as &$button){
				$button["paypal_button_html_modified"] = "";
				$button["paypal_button_html"] = "";
			}
		}
		return $d;
	}

}