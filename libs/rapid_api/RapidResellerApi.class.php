<?php


class RapidResellerApi extends RapidApi{
	
	protected $uri = 'api.rapidshare.com/cgi-bin/rsapi.cgi';
	protected $accountid = 1337; //reseller id
	protected $accountpass = '1337'; //reseller pass
	
	public function getLogs($limit=100){
		return $this->parseResult(
					$this->getResult(
						"resapi_logs_v1", 
						array("limit"=>intval($limit))
					),
					array("timestamp", "IP", "command", "data1", "data2")
				);
	}
	
	
	public function getListOfMyAccounts($limit=100){
		return $this->parseResult(
					$this->getResult(
						"resapi_show_v1", 
						array("limit"=>intval($limit))
					),
					array("accountid", "email", "addtime", "fraud", "enabletime", "enableip")
				);
	}
	
	public function checkAccount($accountid){
		return $this->parseResult(
					$this->getResult(
						"resapi_checkaccount_v1", 
						array("accountid"=>intval($accountid))
					),
					array("accountid")
				);
	}
	
	
	
	/**
	 * if accountid is not given, new account will be created using email
	 */
	public function buyRapids($accountid="", $nrrapids=50, $email=""){
		$retparams = array();
		$sendparams = array();
		if( $accountid == "" && $email != "" ){
			$sendparams = array("email"=>$email, "rapids"=>intval($nrrapids));
			$retparams = array("newaccountid", "newpassword");
		}
		else if( $accountid != "" ){
			$sendparams = array("accountid"=>intval($accountid), "rapids"=>intval($nrrapids));
			$retparams = array("status");
		}
		else
			return false;
		return $this->parseResult(
					$this->getResult(
						"resapi_buyrapids_v1", 
						$sendparams
					),
					$retparams
				);
	}
	
	
	public function changePassword($accountid, $newpasswd){
		return $this->parseResult(
					$this->getResult(
						"resapi_newpassword_v1", 
						array("accountid"=>intval($accountid), "newpassword"=>$newpasswd)
					),
					array("newpassword")
				);
	}
	
	
	public function changeEmail($accountid, $newemail){
		return $this->parseResult(
					$this->getResult(
						"resapi_newemail_v1", 
						array("accountid"=>intval($accountid), "newemail"=>$newemail)
					),
					array("newemail")
				);
	}
	
		
}