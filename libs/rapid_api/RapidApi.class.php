<?php


class RapidApi{
	
	
	
	private $_USE_SECURE_CONNECITON;
	private $_IF_SECURE_USE_CURL;
	
	protected $uri = "";
	protected $accountid = "";
	protected $accountpass = "";
	
	
	
	public function __construct(){
		//always try to get secure connection by default
		$this->useSecureConnection( true );
	}
	
	
	/**
	 * whether to use https or http
	 * @returns true if https can be used of false if not
	 */
	public function useSecureConnection( $secure=true ){
		if( $secure === true ){
			$this->_USE_SECURE_CONNECITON = true;
		
			$w = stream_get_wrappers();
			/*
			echo 'openssl: ',  extension_loaded  ('openssl') ? 'yes':'no', "\n";
			echo 'http wrapper: ', in_array('http', $w) ? 'yes':'no', "\n";
			echo 'https wrapper: ', in_array('https', $w) ? 'yes':'no', "\n";
			echo 'wrappers: ', var_dump($w);
			*/
			
			//can use file_get_contents
			if( in_array('https', $w) )
				$this->_IF_SECURE_USE_CURL = false;
			//can use curl
			if (function_exists('curl_init'))
				$this->_IF_SECURE_USE_CURL = true;
			//fallback to http
			else
				$this->_USE_SECURE_CONNECITON = false;
			
		}
		else
			$this->_USE_SECURE_CONNECITON = false;
			
		
		return $this->_USE_SECURE_CONNECITON === $secure;
	}
	
	
	
	private function buildUrl( $subroutine, $params=array() ){
		//add secret, standard, always used params 
		$params = array_merge( $params, array("login"=>$this->accountid, "password"=>$this->accountpass) );
		
		$paramsstr = ''; 
		foreach( $params as $k=>$v)
			$paramsstr .= "&$k=$v";
		
		return 	($this->_USE_SECURE_CONNECITON ? 'https://' : 'http://')
				. $this->getUri()
				. "?sub=$subroutine"
				. $paramsstr;
	}
	
	protected function getUri(){
		return $this->uri;
	}
	
	protected function getResult( $subroutine, $params=array() ){
		$result = "";
		$url = $this->buildUrl($subroutine, $params);
		//echo $url."<br><br>"; return;
		if( $this->_USE_SECURE_CONNECITON && $this->_IF_SECURE_USE_CURL ){
			// initialize a new curl resource
			$ch = curl_init();
			// set the url to fetch
			curl_setopt($ch, CURLOPT_URL, $url); 
			// don't give me the headers just the content
			curl_setopt($ch, CURLOPT_HEADER, 0); 
			// return the value instead of printing the response to browser
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			// use a user agent to mimic a browser
			//curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5) Gecko/20041107 Firefox/1.0'); 
			$result = curl_exec($ch); 
			// remember to always close the session and free all resources 
			curl_close($ch);
		}
		else
			$result = file_get_contents($url); 
		
		return $result;
	}
	
	
	

	/**
	 * parses the results and returns them in array
	 * if $expectedData is sent and is not empty, its values are used to mapping the results to them
	 */
	protected function parseResult($result, $expectedData=array()){
		$result = trim($result, "\n");
		$status = strtolower(substr($result, 0, 6)) == "error:" ? "error" : "ok";
		$ret = array( "status" => $status );
		if( $status == "ok" ){
		
			$ret['params'] = array();
			if(strpos($result, "=") !== false && strpos($result, "\n") !== false){
				$lines = explode("\n", $result);
				foreach( $lines as $line ){
					$params =  explode('=', $line);
					$ret['params'][$params[0]] = $params[1];
				}
			}
			
			else{
				$lines = str_replace("\n", ",", $result);
				$lines = explode("\n", $lines);
				foreach( $lines as $line ){
					$params =  explode(',', $line);
					$i = 0;
					$p = array();
					foreach( $params as $param )
						$p[@$expectedData[$i] ? $expectedData[$i++] : "param".(++$i)] = $param;
					$ret['params'] = $p;
				}
			}
			
		}
		else
			$ret['error'] = substr($result, 7);
		return $ret;
	}
	
}