<?php


class RapidUserApi extends RapidApi{
	
	protected $uri = 'api.rapidshare.com/cgi-bin/rsapi.cgi';
	protected $accountid = "";
	protected $accountpass = "";
	
	
	
	function __construct($accountId="", $accountpass=""){
		parent::__construct();
		$this->setCredentials($accountId, $accountpass);
	}
	
	
	public function setCredentials($accountId, $accountpass){
		$this->accountid = $accountId;
		$this->accountpass = $accountpass;
		return $this;
	}
	
	public function getAccountDetails(){
		return $this->parseResult(
					$this->getResult(
						"getaccountdetails"
					),
					array()
				);
	}
	
	public function createAccount($username, $email, $pass){
		$this->setCredentials($username, $pass);
		return $this->parseResult(
					$this->getResult(
						"newpremiumaccount",
						array(
							"username" => $username,
							"email" => $email,
							"password" => $pass
						)
					),
					array()
				);
	}
	
	
	public function setAccountDetails($details=array()){
		return $this->parseResult(
					$this->getResult(
						"setaccountdetails",
						$details
					)
				);
	}
	
	public function checkUsernameAvailability($username){
		$_userName = "uSERcheck3rLOL";
		$_userPass = "af23Fwer34AF324";
		$tmpuser = new RapidUserApi($_userName, $_userPass);
		//check if username is available
		$res = $tmpuser->setAccountDetails(array("username" => $username));
		$status  = 1;
		if($res["status"] == "ok"){
			//set it back
			$tmpuser->setCredentials($username, $_userPass);
			$tmpuser->setAccountDetails(array("username" => $_userName));
			return 1;
		}
		$err = strtolower(preg_replace('/\s+/','',$res["error"]));
		if( strpos($err, "usernamealreadyinuse") !== false )
			return $status = 2;
		else{
			Logger::log("fuck, can't check username availability, error='".$res["error"]."'", true, get_class()."->checkUserAvailability()  ");
			return $status = -1;
		}
	}
	
	/**
	 * parses the results and returns them in array
	 * @param $expectedData is ignored here and waits for better times if they'll come.. we dont need it.. fucker! and fuckers are also rapidshare devs for having mess of api and returning results which vary fucking fuckers 
	 */
	protected function parseResult($result, $expectedData=array()){
		$result = trim($result, "\n");
		$status = strtolower(substr($result, 0, 6)) == "error:" ? "error" : "ok";
		$ret = array( "status" => $status );
		if( $status == "ok" ){
		
			$ret['params'] = array();
			if(strpos($result, "=") !== false && strpos($result, "\n") !== false){
				$lines = explode("\n", $result);
				foreach( $lines as $line ){
					$params =  explode('=', $line);
					$ret['params'][$params[0]] = $params[1];
				}
			}
			else{
				$ret['params'] = explode(",", $result);
			}
		}
		else
			$ret['error'] = substr($result, 7);
		return $ret;
	}
}