<?php


/**
 * if no class was found, php will call this function to load the class
 * as final resort before throwing the fatal error
 */
function __autoload($class_name) {
	if( ! KLoader::loadKooreClass($class_name) ){
		
		//maybe it is a model which is not loaded by default via MVCHandler?
		if( KLoader::loadApplicationModel($class_name) === false ){
			
			//maybe it is library
			KLoader::loadLib($class_name);
			
		}
		
	}
	
}





/*
 * some usefull functions that should be available to all the framework
 */


/**
 * recursive glob funciton
 */
/*
function rglob($pattern, $flags = 0, $path = '') {
	if (!$path && ($dir = dirname($pattern)) != '.') {
		if ($dir == '\\' || $dir == '/') $dir = '';
		return rglob(basename($pattern), $flags, $dir . '/');
	}
	$paths = glob($path . '*', GLOB_ONLYDIR | GLOB_NOSORT);
	$files = glob($path . $pattern, $flags);
	foreach ($paths as $p) $files = array_merge($files, rglob($pattern, $flags, $p . '/'));
	return $files;
}
/*/
function rglob($path, $pattern) {
	$files = glob( $path . '/' . $pattern);
	$paths = glob($path . '/*', GLOB_ONLYDIR | GLOB_NOSORT);
	
	$files = is_array($files) ? $files : array();
	
	if( is_array($paths) ){
		foreach( $paths as $file )
			if( is_dir($file) && $file!="." && $file!=".." )
				$files = array_merge($files, rglob($file, $pattern));
	}
	return $files;
}
//*/



/** 
* A recursive array_change_key_case function. 
* @param array $input 
* @param integer $case 
*/ 
function array_change_key_case_recursive($input, $case = null){ 
	if(!is_array($input)){ 
		trigger_error("Invalid input array '{$array}'",E_USER_NOTICE); exit; 
	} 
	// CASE_UPPER|CASE_LOWER 
	if(null === $case){ 
		$case = CASE_LOWER; 
	} 
	if(!in_array($case, array(CASE_UPPER, CASE_LOWER))){ 
		trigger_error("Case parameter '{$case}' is invalid.", E_USER_NOTICE); exit; 
	} 
	$input = array_change_key_case($input, $case); 
	foreach($input as $key=>$array){ 
		if(is_array($array)){ 
			$input[$key] = array_change_key_case_recursive($array, $case); 
		} 
	} 
	return $input; 
}


/**
 * return the name of the parent directory
 */
function basedir( $file ){
	
	$file	= dirname($file);
	
	$pos	= 0;
	
	if( ($pos=strrpos($file, "/")) === false )
		return $file;
	
	return substr($file, $pos+1);
	
}







/**
 * will write an array to a cached file:
 * array(
 *		koore => array(
 *			classes = array( class_name => file, ... ),
 * 			configs = array( file, file, ... )
 *		),
 *		app => array(
 *			controllers => (class_name => file, ... ),
 * 			layouts  => (class_name => file, ... ),
 *			models =>  => (class_name => file, ... )
 * 		)
 * )
 *
 * all koore's classes and configs will be appended to one file which will be included on startup
 * 
 */
class KLoader{
	
	public static $USE_CACHED_CLASSES;
	
	public static $APPLICATION_DIR_NAME;
	
	private static $_CACHE_DIR = "/cache";
	
	private static $_LIBS_DIR = '/libs';
	
	private static $_CACHED_KOORE_CLASSES_AND_CONFIGS = "classes_and_configs";
	
	private static $_CACHED_PATHS = "searialized_paths";
	
	private static $_KOORE_DIR;
	
	private static $doSaveKooreClasses 	= false;
	private static $doSavePaths 		= false;
	
	/*
	 * this wil hold all *.includer.php files in $_LIBS_DIR directory after we will scan that directory 
	 * (this will happen if non included class will be required and it wont be class or model via KLoaders loadLib() method)
	 */
	private static $scannedLibsIncluders = null;
	
	private static $PATHS = array();
	
	
	public static function init($kooreDir, $applicationDirName, $useCachedClasses=true){
	
		self::$USE_CACHED_CLASSES = $useCachedClasses;
		
		self::$_KOORE_DIR = $kooreDir;
		
		self::$APPLICATION_DIR_NAME = $applicationDirName;
		
		self::$_CACHE_DIR = dirname(__FILE__) . self::$_CACHE_DIR;
		
		self::$_CACHED_KOORE_CLASSES_AND_CONFIGS = self::$_CACHE_DIR . '/' . self::$_CACHED_KOORE_CLASSES_AND_CONFIGS;
		
		self::$_CACHED_PATHS = self::$_CACHE_DIR . '/' . self::$_CACHED_PATHS;
		
		
		if( ! is_writable(self::$_CACHE_DIR) )
			throw new Exception("KLoader: problem, cache dir '".self::$_CACHE_DIR."' is not writable!");
		
		self::loadPaths();
		if( self::$USE_CACHED_CLASSES )
			self::loadAllKooreClasses( self::$USE_CACHED_CLASSES );
		
		else
			self::loadKooreConfigs();
		
		
		//print_r(self::$PATHS);
		
		
		
	}
	
	
	
	/**
	 * tells the class that it should make final calls if needed
	 */
	public static function shutDown(){
		
		//save the classes and paths if needed
		
		if( self::$doSavePaths )
			self::savePaths();
		
		if( self::$doSaveKooreClasses )
			self::saveKooreClasses(array(__CLASS__,"Koore"));
			
	}
	
	
	
	public static function loadPaths(){
		
		if( is_file(self::$_CACHED_PATHS) )
			self::$PATHS = unserialize( file_get_contents(self::$_CACHED_PATHS) );
			
		else
			self::$doSavePaths = true;
			
	}
	
	private static function savePaths(){
		file_put_contents( self::$_CACHED_PATHS, serialize(self::$PATHS) );
	}
	
	
	public static function loadAllKooreClasses( $useCachedClasses=true ){
		
		//echo "loading: ALL CLASSES; cached: $useCachedClasses ";
		
		if( $useCachedClasses && is_file(self::$_CACHED_KOORE_CLASSES_AND_CONFIGS) ){
			include(self::$_CACHED_KOORE_CLASSES_AND_CONFIGS);
			return;
		}
		
		else if( $useCachedClasses )
			self::$doSaveKooreClasses = true;
			
		
		if( ! @self::$PATHS['koore']["classes"] )
			self::updatePaths("koore");
			
		foreach( self::$PATHS['koore']["classes"] as $className=>$file )
			include_once($file);
		
		//also load all configs here
		self::loadKooreConfigs();
		
	}
	
	
	public static function loadKooreClass( $className ){
		
		//we have all keys in lower case
		$className = strtolower($className);
		
		//echo "loading: " . $className ;
		//echo self::$PATHS['koore']["classes"][$className];
		
		if( ! @array_key_exists($className, self::$PATHS['koore']["classes"]) ){
			$tmpDoSaveKooreClasses = self::$doSaveKooreClasses;
			$tmpDoSavePaths = self::$doSavePaths;
			self::updatePaths("koore");
			if( ! @array_key_exists($className, self::$PATHS['koore']["classes"]) ){
				//don't update the classes if this class wasn't found, but respect previous decisions if made
				self::$doSaveKooreClasses = $tmpDoSaveKooreClasses || false;
				self::$doSavePaths = $tmpDoSavePaths || false;
				
				return false;
			}
			self::$doSavePaths = true;
		}
		
		$file = self::$PATHS['koore']["classes"][$className];
		
		//was moved?
		if( ! is_file($file) ){
		
			self::updatePaths("koore");
			$file = self::$PATHS['koore']["classes"][$className];
			
			if( ! is_file($file) )
				return false;
		}
		
		include($file);
		
		return true;
		
	}
	
	public static function loadKooreConfigs(){
		
		if( ! @self::$PATHS['koore']["configs"] )
			self::updatePaths("koore");
		
		foreach( self::$PATHS['koore']["configs"] as $c ){
			if( ! is_file($c) ){
				self::updatePaths("koore");
				break;
			}
		}
		
		foreach( self::$PATHS['koore']["configs"] as $c )
			include($c);
	}
	
	public static function loadLib($libName){
		//did we scan the libs directory?
		if(is_null(self::$scannedLibsIncluders)){
			//scann it..
			self::$scannedLibsIncluders = array();
			$libIncluders = array_filter(glob(self::$_KOORE_DIR.self::$_LIBS_DIR.'/*Lib.includer.php'), 'is_file');
			foreach($libIncluders as $lincl){
				$m = array();
				preg_match('/\\'.self::$_LIBS_DIR.'\/([a-zA-Z0-9]*)Lib\.includer\.php/', $lincl, $m);
				if(sizeof($m) > 1)
					self::$scannedLibsIncluders[$m[1]] = $lincl;
			} 
		}
		//if lib exists, load it
		if(array_key_exists($libName, self::$scannedLibsIncluders))
			return include_once(self::$scannedLibsIncluders[$libName]);
		return false;
	}
	
	
	public static function saveKooreClasses($ignoreClasses=array("KLoader","Koore")){
		$first = false;
		
		//just check for maybe moved class
		foreach( self::$PATHS['koore'] as $a ){
			$break = false;
			foreach( $a as $className=>$file ){
				if( ! is_file($file) ){
					$break = true;
					self::updatePaths("koore");
				}
			}
			if( $break )
				break;
		}
		
		//store all classes into one file
		foreach( self::$PATHS['koore'] as $a )
			foreach( $a as $className=>$file ){
				
				//$className . "" <- cast possible integers to string, don't know why but must be so
				if( in_array($className . "", array_map('strtolower', $ignoreClasses)) )
					continue;
					
				if( ! $first ){
					file_put_contents( self::$_CACHED_KOORE_CLASSES_AND_CONFIGS, "<?php\n".trim(file_get_contents($file), '<?php>') );
				//	echo file_get_contents($file);
					$first = true;
				}
				else
					file_put_contents( self::$_CACHED_KOORE_CLASSES_AND_CONFIGS, trim(file_get_contents($file), '<?php>'), FILE_APPEND );
			}
				
	}
	
	
	public static function loadApplicationModel($className){
		return self::loadApplicationClass(str_replace("Model","",$className), "models");
	}
	public static function loadApplicationController($className){
		return self::loadApplicationClass(str_replace("Controller","",$className), "controllers");
	}
	public static function loadApplicationLayout($className){
		return self::loadApplicationClass(str_replace("Layout","",$className), "layouts");
	}
	
	
	private static function loadApplicationClass($className, $which){
		$tmpDoSaveKooreClasses = self::$doSaveKooreClasses;
		$tmpDoSavePaths = self::$doSavePaths;
			
		//we have all keys in lower case
		$className = strtolower($className);
		
		if( ! @self::$PATHS['app'][$which] )
			self::updatePaths($which);
		
		else if( ! @self::$PATHS['app'][$which][$className] )
			self::updatePaths($which);
		
		if( ! @self::$PATHS['app'][$which][$className] )
			return false;
		
		//maybe file was moved and paths were not updated yet
		if( ! is_file(self::$PATHS['app'][$which][$className]) ){
			self::updatePaths($which);
			
			//if file was removed and we want it but it doesnt exist anymore
			if( ! is_file(self::$PATHS['app'][$which][$className]) ){
				//don't save anything if it wasn't found
				self::$doSaveKooreClasses = $tmpDoSaveKooreClasses || false;
				self::$doSavePaths = $tmpDoSavePaths || false;
				
				return false;
			}
		}
		
		include( self::$PATHS['app'][$which][$className] );
		
		//return the name of this class, based on file name!
		return basename(self::$PATHS['app'][$which][$className], '.class.php');
	}
	
	
	
	public static function getLayouts(){
		return self::getApplicationClassesArray("layouts");
	}
	public static function getModels(){
		return self::getApplicationClassesArray("models");
	}
	public static function getControllers(){
		return self::getApplicationClassesArray("controllers");
	}
	
	private static function getApplicationClassesArray( $which ){
		$r = self::$PATHS['app'][$which];
		
		array_walk(
			$r, 
			create_function('&$value, $key', '$value = basename($value, ".class.php");')
		);
		
		return $r;
	}
	
	
	
	private static function updatePaths($which){
		
		switch($which){
		case "all":
			self::updatePaths("koore");
			self::updatePaths("controllers");
			self::updatePaths("models");
			self::updatePaths("layouts");
		break;
		case "koore":
			self::updateKooreConfigPaths();
			self::updateKooreClassPaths();
		break;
		case "controllers":
		case "models":
		case "layouts":
			self::updateApplicationPaths( $which );
		break;
		}
		
		self::$PATHS = array_change_key_case_recursive(self::$PATHS, CASE_LOWER);
		
	}
	
	
	
	
	private static function updateKooreConfigPaths(){
		$t = &self::$PATHS;
		
		if( ! array_key_exists("koore",$t) || ! is_array($t["koore"]) )
			$t['koore'] = array();
		
		//if( ! array_key_exists("configs",$t['koore']) || ! is_array($t['koore']["configs"]) )
		//make new empty array
			$t['koore']['configs'] = array();
			
		$t['koore']['configs'] = array_filter( rglob( self::$_KOORE_DIR, "*.config.php"), 'is_file');
		
		self::$doSavePaths = true;
		self::$doSaveKooreClasses = true;
	}
	
	
	private static function updateKooreClassPaths(){
		$t = &self::$PATHS;
		
		if( ! array_key_exists("koore",$t) || ! is_array($t["koore"]) )
			$t['koore'] = array();
			
		//if( ! array_key_exists("classes",$t['koore']) || ! is_array($t['koore']["classes"]) )
		//make new empty array
			$t['koore']['classes'] = array();
		
		$classes = array_filter( rglob( self::$_KOORE_DIR, "*.class.php"), 'is_file');
		
		foreach( $classes as $class )
			$t['koore']['classes'][str_replace(".class.php", "", basename($class))] = $class;
			
		self::$doSaveKooreClasses = true;
	}
	
	
	private static function updateApplicationPaths( $which ){
	
		if( ! is_dir(realpath(self::$_KOORE_DIR . "/../" . self::$APPLICATION_DIR_NAME)) ){
			throw new Exception( "KLoader: updateApplicationPaths - '".self::$_KOORE_DIR . "/../" . self::$APPLICATION_DIR_NAME ."' is not a directory" );
		}
		
		
		switch($which){
		case "all":
			self::updateApplicationPaths("controllers");
			self::updateApplicationPaths("models");
			self::updateApplicationPaths("layouts");
			return;
		break;
		case "controllers":
		case "models":
		case "layouts":
		break;
		}
		
		$suffix = self::getSuffixForClassType($which);
		
		
		$t = &self::$PATHS;
		
		if( ! array_key_exists("app",$t) || ! is_array($t["app"]) )
			$t['app'] = array();
			
		//if( ! array_key_exists($which, $t['app']) || ! is_array($t['app'][$which]) )
		//make new empty array
			$t['app'][$which] = array();
		
		
		$classes = array_filter( rglob( realpath(self::$_KOORE_DIR . "/../". self::$APPLICATION_DIR_NAME), "*".$suffix), 'is_file');
		 
		foreach( $classes as $class )
			$t['app'][$which][str_replace($suffix, "", basename($class))] = $class;
			
		self::$doSavePaths = true;
		
	}
	
	
	private static function getSuffixForClassType( $which ){
		$suffix;
		switch($which){
		case "controllers":
			$suffix = "Controller.class.php";
		break;
		case "models":
			$suffix = "Model.class.php";
		break;
		case "layouts":
			$suffix = "Layout.class.php";
		break;
		}
		
		return $suffix;
	}
	
}















